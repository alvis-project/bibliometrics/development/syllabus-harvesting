import http
import time
from random import randint

import json
import urllib
from urllib.request import urlopen


class Harvester(object):

    def __init__(self, base_url, organisation):
        self.courses_url = base_url + '?institution=' + organisation
        self.reading_list_url = base_url + '/{}/reading-lists?institution=' + organisation
        self.citations_url = base_url + '/{}/reading-lists/{}?institution=' + organisation + \
                             '&view=full'

    def get_courses(self, start, how_many):
        """Get a list of courses within a range defined by start and how many to retrieve."""
        course_url_with_offset = self.courses_url + '&limit={}&offset={}'
        with urlopen(course_url_with_offset.format(how_many, start)) as connection:
            return json.loads(connection.read().decode("utf-8"))

    def get_reading_lists(self, course_id):
        url = self.reading_list_url.format(course_id)
        with urlopen(url) as connection:
            val = json.loads(connection.read().decode("utf-8"))
        return val

    def get_citations(self, course_id, reading_list_id, attempt=1):
        payload = ''
        connection = None
        if attempt > 8:
            return {}, -1
        elif attempt != 1:
            wait = randint(2, attempt * 3)
            print('\t\tBacking off. Waiting [{}] seconds'.format(wait))
            time.sleep(wait + attempt)

        try:
            connection = urlopen(self.citations_url.format(course_id, reading_list_id))
            payload = json.loads(connection.read().decode("utf-8"))
        except (http.client.IncompleteRead, urllib.error.HTTPError) as e:
            print("Error reading from https server " + str(e))
            print("Attempting retry [{}] ".format(str(attempt)) + self.citations_url
                  .format(course_id, reading_list_id))
            return self.get_citations(course_id, reading_list_id, attempt + 1)
        return payload, connection.status

    def get_courses_total(self):
        with urlopen(self.courses_url) as connection:
            courses = json.loads(connection.read().decode("utf-8"))
            return courses['total_record_count']
