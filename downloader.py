import getopt
import re
from os.path import join
from datetime import datetime

import json
import os
import sys
import time

from constant import RAW, CITATIONS, READING_LISTS, COURSES, ALL
from file.JsonFile import JsonFile
from harvester import Harvester
from utility.leganto_constants import check_for_unknown_course_keys, \
    check_for_unknown_reading_list_keys, check_for_unknown_citation_keys


def handle_courses(organisation, courses, file):
    for course in courses['course']:
        check_for_unknown_course_keys(course)
        file.write_course(course)
        print("Downloaded [{}] course with code [{}], name [{}] "
              .format(organisation, course['code'], course['name']))


def handle_reading_lists_raw(reading_lists, file, course_id):
    blocked_count = 0
    if 'reading_list' in reading_lists and reading_lists['reading_list'] is not None:
        for reading_list in reading_lists['reading_list']:
            check_for_unknown_reading_list_keys(reading_list)
            if 'id' in reading_list.keys():
                file.write_reading_list(course_id, reading_list)
                print("\tDownloaded reading list with code [{}], name [{}] ".format(
                    reading_list['code'], reading_list['name']))
            else:
                print("\tReading list with code [{}], not available. Visibility [{}] "
                      "".format(reading_list['code'], reading_list['visibility']['value']))
                blocked_count = blocked_count + 1
        print("\t [{}], reading lists are unavailable ".format(blocked_count))


def handle_citations_raw(citations, file, course_id, reading_list):
    # Make sure there is nothing unexpected
    if 'citations' in citations.keys():
        for citation in citations['citations']:
            check_for_unknown_citation_keys(citation)
        file.write_citations(course_id, reading_list['id'], citations)
        if 'citation' in citations['citations'].keys():
            return len(citations['citations']['citation'])
    else:
        print("\tNo citations for course id [{}], reading list id [{}], name [{}]"
              .format(course_id, reading_list['id'], reading_list['name']))

    return 0


def download_courses(organisation, harvester, file, start, end, step):
    """Get a list of courses within a range (from start to end) where step defines how many to
    retrieve. per call. Note. The API should have no problem retrieving everything quickly,
    but we believe it is courteous to not rush the API and perform the harvesting at times when
    the API is not in use (e.g. evenings and weekends)"""

    offset = 0
    for offset in range(int(start), int(end), int(step)):
        handle_courses(organisation, harvester.get_courses(offset, step), file)
        time.sleep(.2)
    # Check that we capture all courses, e.g., with incoming values 0, 43, 5 then we need to get
    # from 40 to 43
    if int(end) != int(offset):
        step = end - offset
        handle_courses(organisation, harvester.get_courses(offset, step), file)


def download_reading_lists(organisation, harvester, file):
    counter = 0
    course_file_directory = file.get_course_directory()
    files = os.listdir(course_file_directory)
    for a_file in files:
        try:
            with open(join(course_file_directory, a_file), 'r') as course_file:
                course = json.loads(course_file.read())
            print("Downloading reading lists for [{}] course # [{}], [{}] with name [{}]"
                  .format(organisation, str(counter), course['code'], course['name']))
            reading_lists = harvester.get_reading_lists(course['id'])
            handle_reading_lists_raw(reading_lists, file, course['id'])
            counter = counter + 1
            time.sleep(.2)
        except OSError as e:
            print("Problem reading course file [{}]. [{}]".format(file, e.strerror))

    print("Downloaded [{}] reading lists for [{}]".format(str(counter), organisation))


def get_course_id_from_link(link):
    match = re.search(r"^.*/courses/(\d+)/reading-lists/\d+$", link)
    if match is not None and len(match.groups()) == 1:
        return match.group(1)
    else:
        raise "Stopping due problem identifying course id from [{}]".format(link)


def get_reading_list_id_from_link(link):
    match = re.search(r"^.*/courses/\d+/reading-lists/(\d+)$", link)
    if match is not None and len(match.groups()) == 1:
        return match.group(1)
    else:
        raise "Stopping due problem identifying course id from [{}]".format(link)


def download_citations(organisation, harvester, file):
    counter = 0
    reading_list_file_directory = file.get_reading_list_directory()
    files = os.listdir(reading_list_file_directory)
    for a_file in files:
        try:
            with open(join(reading_list_file_directory, a_file), 'r') as reading_list_file:
                reading_list = json.loads(reading_list_file.read())
                course_id = get_course_id_from_link(reading_list['link'])
                print("\tDownloading [{}] citations for course id [{}], reading list id [{}], "
                      "name [{}]".format(organisation, course_id, reading_list['id'],
                                         reading_list['name']))

                # Only attempt to download if citations have not already been downloaded
                if file.citations_json_file_exists(course_id, reading_list['id']):
                    print('\t\tAlready downloaded')
                    continue

                citations, status = harvester.get_citations(course_id, reading_list['id'])
                # Ignoring status at the moment
                if status == 200:
                    number_citations = handle_citations_raw(citations, file, course_id,
                                                            reading_list)
                    print("\t[{}] [{}] citations downloaded for course id [{}], reading list id ["
                          "{}], name [{}]".format(str(number_citations), organisation, course_id,
                                                  reading_list['id'], reading_list['name']))
                    counter = counter + 1
                    time.sleep(.8)
        except OSError as e:
            print("Problem reading reading_list file [{}]. [{}]".format(file, e.strerror))


def main(argv):
    server_url = 'https://gw-unit.intark.uh-it.no/leganto-open-api/courses/'

    entity_to_download, start, end, step, organisation_to_process, working_dir = \
        get_options_retrieve(argv)

    if organisation_to_process == ALL:
        organisations = ['aho', 'dmmh', 'forsvaret', 'himolde', 'hivolda', 'hiof', 'hvl', 'inn',
                         'kristiania', 'krus', 'ldh', 'mf', 'nla', 'nhh', 'nmbu', 'nord', 'ntnu',
                         'oslomet', 'phs', 'samas', 'uia', 'uib', 'uio', 'uis', 'uit', 'usn', 'vid']
    else:
        organisations = [organisation_to_process]

    for organisation in organisations:
        # Handle paging as API has max 100 per page
        harvester = Harvester(server_url, organisation)
        file = JsonFile(organisation, working_dir, RAW)

        if entity_to_download == COURSES or entity_to_download is None:
            total_courses = harvester.get_courses_total()
            print(organisation + " has " + str(total_courses) + " courses")
            end = total_courses
            download_courses(organisation, harvester, file, start, end, step)
        elif entity_to_download == READING_LISTS:
            download_reading_lists(organisation, harvester, file)
        elif entity_to_download == CITATIONS:
            download_citations(organisation, harvester, file)
        else:
            print("Unknown option [{}]. Unable to continue.".format(entity_to_download))
            sys.exit()
        print("Finished processing for [{}]".format(organisation))


def get_options_retrieve(argv):
    try:
        # Set some default values that will be used unless they are overridden by command line
        entity = COURSES
        start = 0
        stop = 1000
        step = 100
        organisation = ALL
        # Using year/month as directory as download processing of everything can go over a number
        # of days. Expected use of the script i bi-yearly.
        working_dir = str(datetime.today().year) + "-" + str(datetime.today().month)
        opts, args = getopt.getopt(argv, 'he:o:s:t:p:w:')
        for opt, arg in opts:
            if opt == '-h':
                print(
                    'downloader.py -e <entity> -s <start> -t <stop> -p <step> -o <organisation> -w '
                    '<working-dir>')
                sys.exit()
            if opt in '-e':
                entity = arg
            if opt in '-s':
                start = arg
            if opt in '-t':
                stop = arg
            if opt in '-p':
                step = arg
            if opt in '-o':
                organisation = arg
            if opt in '-w':
                working_dir = arg

        return entity, int(start), int(stop), int(step), organisation, working_dir

    except getopt.GetoptError:
        print('downloader.py -e <entity> -s <start> -t <stop> -p <step> -o <organisation> -w '
              '<working-dir>')
        sys.exit(2)


if __name__ == '__main__':
    main(sys.argv[1:])
    sys.exit()
