
def get_string_value_dict(a_dict, key):
    """Get a string value from a dict with check key existence to avoid missing key problem."""
    if key in a_dict:
        return get_string_value_or_null(a_dict[key])
    return None


def get_string_value_sub_dict(a_dict, key_1, key_2):
    """Get a string value from a 2D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and a_dict[key_1] is not None and key_2 in a_dict[key_1]:
        return get_string_value_or_null(a_dict[key_1][key_2])
    return None


def get_string_value_sub_sub_dict(a_dict, key_1, key_2, key_3):
    """Get a string value from a 3D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and key_2 in a_dict[key_1] and key_3 in a_dict[key_1][key_2]:
        return get_string_value_or_null(a_dict[key_1][key_2][key_3])
    return None


def get_numeric_value_dict(a_dict, key_):
    """Get a numeric value from a dict with check key existence to avoid missing key problem."""
    if key_ in a_dict:
        return get_numeric_value_or_null(a_dict[key_])
    return None


def get_numeric_value_sub_dict(a_dict, key_1, key_2):
    """Get a numeric value from a 2D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and key_2 in a_dict[key_1]:
        return get_numeric_value_or_null(a_dict[key_1][key_2])
    return None


def get_numeric_value_sub_sub_dict(a_dict, key_1, key_2, key_3):
    """Get a numeric value from a 3D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and key_2 in a_dict[key_1] and key_3 in a_dict[key_1][key_2]:
        return get_numeric_value_or_null(a_dict[key_1][key_2][key_3])
    return None


def get_boolean_value_dict(a_dict, key_):
    """Get a boolean value from a dict with check key existence to avoid missing key problem."""
    if key_ in a_dict:
        value = get_numeric_value_or_null(a_dict[key_])
        if value is not None:
            return bool(value * 1)
    return None


def get_boolean_value_sub_dict(a_dict, key_1, key_2):
    """Get a boolean value from a 2D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and key_2 in a_dict[key_1]:
        value = get_numeric_value_or_null(a_dict[key_1][key_2])
        if value is not None:
            return bool(value * 1)
    return None


def get_boolean_value_sub_sub_dict(a_dict, key_1, key_2, key_3):
    """Get a boolean value from a 3D dict with check key existence to avoid missing key problem."""
    if key_1 in a_dict and key_2 in a_dict[key_1] and key_3 in a_dict[key_1][key_2]:
        value = get_numeric_value_or_null(a_dict[key_1][key_2][key_3])
        if value is not None:
            return bool(value * 1)
    return None


def get_string_value_or_null(value):
    """Add a string value for an insert query. Some numbers are given as string."""
    if value is not None and value != '':
        return str(value)
    return None


def get_numeric_value_or_null(value):
    """Add a numeric value for an insert query. Some numbers are given as string."""
    if value is not None:
        return value
    return 'null'

known_tables = ['course', 'course_reading_list',  'course_citation', 'counters',
                'course_searchable_id', 'course_term', 'organisations']

IDX_organisation = 0
IDX_COURSE_ID = 2
IDX_CITATIONS_HARVESTED = 18
PROCESS = 'process'
DROP = 'drop'
DROP_CREATE = 'drop-create'
CREATE = 'create'
COURSES = 'courses'
READING_LISTS = 'reading_lists'
CITATIONS = 'citations'
PLANS = 'study_plans'
HTML = 'html'
RAW = 'raw'
PROCESSED = 'processed'
UNKNOWN = 'unknown'
ALL = 'all'
JSON = 'json'
CSV = 'csv'
DB = 'db'
