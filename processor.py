import getopt
import os
import sys

from constant import ALL
from course_plans.OsloMetDownloader import OsloMetDownloader
from course_plans.UiBDownloader import UiBDownloader


def main(argv):
    organisation_to_process, db_file, working_dir = get_options_retrieve(argv)

    if organisation_to_process == ALL:
        organisations = os.listdir(working_dir)
    else:
        organisations = [organisation_to_process]

    organisations = ['uib']

    for organisation in organisations:
        if organisation == 'oslomet':
            downloader = OsloMetDownloader(organisation, db_file, working_dir)
        elif organisation == 'uib':
            downloader = UiBDownloader(organisation, db_file, working_dir)


        downloader.process()

    sys.exit(0)


def get_options_retrieve(argv):
    try:
        # Assume all organisations to be processed
        organisation = ALL
        organisation = 'oslomet'

        working_dir = '../leganto'
        # Assume syllabus.sqlite as default file
        db_file = '../syllabus.sqlite'
        opts, args = getopt.getopt(argv, 'hc:i:b:w', ['organisation=', 'db-file=', 'working-dir='])
        for opt, arg in opts:
            if opt == '-h':
                print('download-plans.py -i <organisation>  -b <database-file>')
                sys.exit()
            if opt in ('-i', '--organisation'):
                organisation = arg
            if opt in ('-b', '--database-file'):
                db_file = arg
            if opt in ('-d', '--working-dir'):
                working_dir = arg

        return organisation, db_file, working_dir

    except getopt.GetoptError:
        print('db-utils.py -c <command> -i <organisation>  -b <database-file> -d  -b <working-dir>')
        sys.exit(2)


if __name__ == '__main__':
    main(sys.argv[1:])


