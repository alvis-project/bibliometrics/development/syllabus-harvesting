
# Transforming code

If there is no academic department identified, the cristin organisation number is applied. 

Most organisations use, what looks like, an identifier from cristin data. In most cases the 
identifier uses an underscore instead of a full stop. The scripts here transform the underscore 
to a full stop so that further processing against cristin is done use keys that follow the same 
format. Two organisation UiT and UiB use cristin numbers, but appear as a single digit e.g., 
186321700. This number needs to be processed to be 186.32.17.0.  

To transform the cristin file use the following jq command:

> cat uit_cristin.json | jq '[.[] | {(.unit_name.nb|tostring): {id: .cristin_unit_id}}] | sort_by((.unit_name.nb|tostring))' > uit_cristin_id_list.json  

## UiT Notes

The source for the UiT cristin data is:

> https://api.cristin.no/v2/units?institution=186&per_page=1000
 
The file `uit_cristin.json` contains the contents taken from the API corresponding to the commit 
time of the file. 

> {
    "Institutt for ingeniørvitenskap og sikkerhet": {
      "id": "186.32.17.0"
    }
  },


>{
    "RKBU Nord": {
      "id": "186.31.91.0"
    }
  },

>{
    "Senter for fredsstudier": {
      "id": "186.33.82.0"
    }
  },
  
needs to be manually entered. It seems the cristin data is not updated with the correct value. 
This is a temporal issue as department names change over time. The important thing is to make 
sure you have an identifier, the name is not that important for analysis. 

The UiT data also includes one record for a course:

> SOS-1012, Kultur, fritid og frivillig arbeid

that does not have a department name associated with it. I currently do not think it is 
worthwhile capturing this single record code wise, but if more of these type of records appear 
in other organisations it may be worthwhile capturing them.

## UiB Notes

The source for the UiT cristin data is:

> https://api.cristin.no/v2/units?institution=186&per_page=1000

To transform the cristin file use the following two jq commands:

> cat uib_cristin.json | jq '[.[] | select(.unit_name.nb != null ) | {(.unit_name.nb|tostring): {id: .cristin_unit_id}}]' > uib_no.json  
> cat uib_cristin.json | jq '[.[] | select(.unit_name.en != null ) | {(.unit_name.en|tostring): {id: .cristin_unit_id}}]' > uit_en.json

The two files can be manually merged into `uib_cristing_id_list.json`. It is likely that jq is 
able to handle the identification of .unit_name.nb and .unit_name.en, but time constraints leave us
having to deal with it manually. Alternatively we could write a simple python script that took care 
of it.

Note: 184100000 has no academic_department_code that exists. The translation to 184.10.0.0 
therefor creates a link to something that does not exist. I am unsure if this is the correct 
strategy. From my understanding 184100000 is a non-existing unit that spans all faculties.

The following values have to be added to ``

> {
    "Examen philosophicum": {
      "id": "184.10.0.0"
    }
  },
  {
    "Det humanistiske fakultet": {
      "id": "184.11.0.0"
    }
  },
  {
    "Institutt for fremmedspråk": {
      "id": "184.11.20.0"
    }
  },
  {
    "Institutt for lingvistiske, litterære og estetiske studier": {
      "id": "184.11.21.0"
    }
  },
  {
    "Institutt for arkeologi, historie, kultur- og religionsvitenskap": {
      "id": "184.11.22.0"
    }
  },
  {
    "Institutt for filosofi og førstesemesterstudier": {
      "id": "184.11.62.0"
    }
  },
  {
    "Senter for kvinne- og kjønnsforskning": {
      "id": "184.11.74.0"
    }
  },
  {
    "Senter for vitenskapsteori": {
      "id": "184.11.75.0"
    }
  },
  {
    "Det matematisk-naturvitenskapelige fakultet": {
      "id": "184.12.0.0"
    }
  },
  {
    "Matematisk institutt": {
      "id": "184.12.11.0"
    }
  },
  {
    "Institutt for informatikk": {
      "id": "184.12.12.0"
    }
  },
  {
    "Institutt for fysikk og teknologi": {
      "id": "184.12.24.0"
    }
  },
  {
    "Kjemisk institutt": {
      "id": "184.12.31.0"
    }
  },
  {
    "Geofysisk institutt": {
      "id": "184.12.44.0"
    }
  },
  {
    "Institutt for geovitenskap": {
      "id": "184.12.50.0"
    }
  },
  {
    "Det medisinske fakultet": {
      "id": "184.13.0.0"
    }
  },
  {
    "Institutt for biomedisin": {
      "id": "184.13.14.0"
    }
  },
  {
    "Institutt for klinisk odontologi": {
      "id": "184.13.19.0"
    }
  },
  {
    "Klinisk institutt 1": {
      "id": "184.13.24.0"
    }
  },
  {
    "Klinisk institutt 2": {
      "id": "184.13.25.0"
    }
  },
  {
    "Institutt for global helse og samfunnsmedisin": {
      "id": "184.13.26.0"
    }
  },
  {
    "Det samfunnsvitenskapelige fakultet": {
      "id": "184.15.0.0"
    }
  },
  {
    "Sosiologisk institutt": {
      "id": "184.15.11.0"
    }
  },
  {
    "Institutt for administrasjon og organisasjonsvitenskap": {
      "id": "184.15.12.0"
    }
  },
  {
    "Institutt for sammenliknende politikk": {
      "id": "184.15.13.0"
    }
  },
  {
    "Institutt for økonomi": {
      "id": "184.15.15.0"
    }
  },
  {
    "Institutt for sosialantropologi": {
      "id": "184.15.34.0"
    }
  },
  {
    "Institutt for geografi": {
      "id": "184.15.41.0"
    }
  },
  {
    "Det juridiske fakultet": {
      "id": "184.16.0.0"
    }
  },
  {
    "Det psykologiske fakultet": {
      "id": "184.17.0.0"
    }
  },
  {
    "Institutt for biologisk og medisinsk psykologi": {
      "id": "184.17.32.0"
    }
  },
  {
    "HEMIL-senteret": {
      "id": "184.17.33.0"
    }
  },
  {
    "Institutt for klinisk psykologi": {
      "id": "184.17.34.0"
    }
  },
  {
    "Institutt for samfunnspsykologi": {
      "id": "184.17.35.0"
    }
  },
  {
    "Institutt for pedagogikk": {
      "id": "184.17.42.0"
    }
  },
  {
    "The Centre for the Science of Learning & Technology (SLATE)": {
      "id": "184.17.50.0"
    }
  },
  {
    "Fakultet for kunst, musikk og design": {
      "id": "184.18.0.0"
    }
  },
  {
    "Institutt for kunst": {
      "id": "184.18.10.0"
    }
  },
  {
    "Institutt for design": {
      "id": "184.18.20.0"
    }
  },
  {
    "Griegakademiet - Institutt for musikk": {
      "id": "184.18.30.0"
    }
  },
  {
    "Forsknings- og innovasjonsavdelingen": {
      "id": "184.21.60.0"
    }
  },
  {
    "AHKR": {
      "id": "AHKR"
    }
  },
  {
    "ECON": {
      "id": "ECON"
    }
  },
  {
    "GEOGRAFI": {
      "id": "GEOGRAFI"
    }
  },
  {
    "INFOMEDIA": {
      "id": "INFOMEDIA"
    }
  },
  {
    "SAMPOL": {
      "id": "SAMPOL"
    }
  },


## him notes

In some cases, it was not possible (due to time constraints ) to handle some variations. 
Examples inlude  
* VeM17 - Bachelor i vernepleie
* DVeM18 - Bachelor i vernepleie - Deltid
* 
It was decided to leave these as they are
