""" Utility functions to improve readability. The code here is related to the processing of
   leganto sources"""
import json
from hashlib import sha256

from constant import get_string_value_dict, get_numeric_value_dict, get_string_value_sub_dict, \
    get_boolean_value_dict, get_string_value_sub_sub_dict, get_boolean_value_sub_dict


def get_course_citation_column_names():
    """Get list of column names applicable for a citation insertion."""
    return "organisation, citation_id, status_value, status_desc, copyrights_status_value, " \
           "copyrights_status_desc, secondary_type_value, secondary_type_desc, type_value, " \
           "type_desc, metadata_title, metadata_author, metadata_publisher, " \
           "metadata_publication_date, metadata_edition, metadata_isbn, metadata_issn, " \
           "metadata_mms_id, metadata_additional_person_name, metadata_place_of_publication, " \
           "metadata_call_number, metadata_note, metadata_journal_title, metadata_article_title, " \
           "metadata_issue, metadata_editor, metadata_chapter, metadata_chapter_title, " \
           "metadata_chapter_author, " \
           "metadata_year, metadata_pages, metadata_source, metadata_series_title_number, " \
           "metadata_pmid, metadata_doi, metadata_volume,  metadata_start_page, " \
           "metadata_end_page,  metadata_start_page2, metadata_end_page2, " \
           "metadata_author_initials, metadata_part, public_note, " \
           "source1, source2, source3, source4, source5, source6, source7, source8, source9, " \
           "source10, last_modified_date, section_info_id, section_info_name, " \
           "section_info_description, section_info_visibility, section_info_tags_total_rec_cnt, " \
           "section_info_section_tags_link, section_info_section_locked, " \
           "file_link, fk_reading_list_id, fk_course_id, org_code, faculty_code, " \
           "institute_code_a, institute_code_b, semester_code_1, semester_code_2, " \
           "semester_code_3, term_code_1, term_name_1, term_code_2, term_name_2, term_code_3, " \
           "term_name_3, year"


def get_course_column_names():
    """Get list of column names applicable for a course insertion."""
    return "organisation, course_id, course_code, course_name, section, " \
           "academic_department_code, academic_department_name, term_code_1, term_name_1, " \
           "term_code_2, term_name_2, term_code_3, term_name_3, " \
           "start_date, end_date, weekly_hours, participants, year, created_date, " \
           "last_modified_date, rolled_from, course_leganto_code, " \
           "org_code, faculty_code, institute_code_a, institute_code_b, status"


def get_course_searchable_id_column_names():
    """Get list of column names applicable for a search id insertion."""
    return "organisation, fk_course_id, searchable_id"


def get_counters_column_names():
    """Get list of column names applicable for counters for an organisation ."""
    return "organisation, courses, reading_lists, citations, blocked_reading_list"


def get_course_reading_list_column_names():
    """Get list of column names applicable for a reading list insertion."""
    return "organisation, reading_list_id, code, name, reading_list_order, description, " \
           "last_modified_date, status_value, status_desc, visibility_value, visibility_desc, " \
           "publishing_status_value, publishing_status_desc, term_code_1, term_name_1, " \
           "term_code_2, term_name_2, term_code_3, term_name_3, " \
           "year, fk_course_id, blocked"


def get_course_value_placeholder():
    """Get list of placeholder for course."""
    return (
            "?, " +  # organisation,
            "?, " +  # id/course_id,
            "?, " +  # name/course_code
            "?, " +  # code/course_name
            "?, " +  # section/section
            "?, " +  # academic_department.value/academic_department_code
            "?, " +  # academic_department.desc/academic_department_name
            "?, " +  # term[0].value/term_code_1
            "?, " +  # term[0].desc/term_name_1
            "?, " +  # term[1].value/term_code_2
            "?, " +  # term[1].desc/term_name_2
            "?, " +  # term[2].value/term_code_3
            "?, " +  # term[3].desc/term_name_3
            "?, " +  # start_date/start_date
            "?, " +  # end_date/end_date
            "?, " +  # weekly_hours/weekly_hours
            "?, " +  # participants/participants
            "?, " +  # year/year
            "?, " +  # created_date/created_date
            "?, " +  # last_modified_date/last_modified_date
            "?, " +  # rolled_from/rolled_from
            "?, " +  # course_leganto_code
            "?, " +  # org_code
            "?, " +  # faculty_code
            "?, " +  # institute_code_a
            "?, " +  # institute_code_b
            "?")  # status
    # //@formatter:off
    # //@formatter:on


def get_reading_list_value_placeholder():
    """Get list of placeholder for reading list."""
    # //@formatter:off
    return (
        "?, " +  # organisation,
        "?, " +  # id/reading_list_id,
        "?, " +  # code/course_code
        "?, " +  # name/course_name
        "?, " +  # order/order
        "?, " +  # description/description
        "?, " +  # last_modified_date/last_modified_date
        "?, " +  # status_value
        "?, " +  # status_desc
        "?, " +  # visibility_value
        "?, " +  # visibility_desc
        "?, " +  # publishing_status_value
        "?, " +  # publishing_status_desc
        "?, " +  # term_code_1
        "?, " +  # term_name_1
        "?, " +  # term_code_2
        "?, " +  # term_name_2
        "?, " +  # term_code_3
        "?, " +  # term_name_3
        "?, " +  # year
        "?, " +  # fk_course_id
        "?")  # blocked
    # //@formatter:on


def get_citation_value_placeholder():
    """Get list of placeholder for a citation."""
    # //@formatter:off
    return (
        "?, " +  # organisation,
        "?, " +  # id/citation_id,
        "?, " +  # status.value/status_value
        "?, " +  # status.desc/status_desc
        "?, " +  # copyrights_status.value/copyrights_status_value
        "?, " +  # copyrights_status.desc/copyrights_status_desc
        "?, " +  # secondary_type.value/secondary_type_value
        "?, " +  # secondary_type.desc/secondary_type_desc
        "?, " +  # type.value/type_value
        "?, " +  # type.desc/type_desc
        "?, " +  # metadata.title/metadata_title
        "?, " +  # metadata.author/metadata_author
        "?, " +  # metadata.publisher/metadata_publisher
        "?, " +  # metadata.publication_date/metadata_publication_date
        "?, " +  # metadata.edition/metadata_edition
        "?, " +  # metadata.isbn/metadata_isbn
        "?, " +  # metadata.issn/metadata_issn
        "?, " +  # metadata.mms_id/metadata_mms_id
        "?, " +  # metadata.additional_person_name/metadata_additional_person_name
        "?, " +  # metadata.place_of_publication/metadata_place_of_publication
        "?, " +  # metadata.call_number/metadata_call_number
        "?, " +  # metadata.note/metadata_note
        "?, " +  # metadata.journal_title/metadata_journal_title
        "?, " +  # metadata.article_title/metadata_article_title
        "?, " +  # metadata.issue/metadata_issue
        "?, " +  # metadata.editor/metadata_editor
        "?, " +  # metadata.chapter/metadata_chapter
        "?, " +  # metadata.chapter_title/metadata_chapter_title
        "?, " +  # metadata.chapter_author/metadata_chapter_author
        "?, " +  # metadata.year/metadata_year
        "?, " +  # metadata.pages/metadata_pages
        "?, " +  # metadata.source/metadata_source
        "?, " +  # metadata.series_title_number/metadata_series_title_number
        "?, " +  # metadata.pmid/metadata_pmid
        "?, " +  # metadata.doi/metadata_doi
        "?, " +  # metadata.volume/metadata_volume
        "?, " +  # metadata.start_page/metadata_start_page
        "?, " +  # metadata.end_page/metadata_end_page
        "?, " +  # metadata.start_page2/metadata_start_page2
        "?, " +  # metadata.end_page2/metadata_end_page2
        "?, " +  # metadata.author_initials/metadata_author_initials
        "?, " +  # metadata.part/metadata_part
        "?, " +  # public_note/public_note
        "?, " +  # source1/source1
        "?, " +  # source2/source2
        "?, " +  # source3/source3
        "?, " +  # source4/source4
        "?, " +  # source5/source5
        "?, " +  # source6/source6
        "?, " +  # source7/source7
        "?, " +  # source8/source8
        "?, " +  # source9/source9
        "?, " +  # source10/source10
        "?, " +  # last_modified_date/last_modified_date
        "?, " +  # section_info.id/section_info_id
        "?, " +  # section_info.name/section_info_name
        "?, " +  # section_info.description/section_info_description
        "?, " +  # section_info.visibility/section_info_visibility
        "?, " +  # section_info.section_tags.total_record_count/section_info_tags_total_rec_cnt
        "?, " +  # section_info.section_tags.link/section_info_section_tags_link
        "?, " +  # section_info.section_locked/section_info_section_locked
        "?, " +  # file_link/file_link
        "?, " +  # fk_reading_list_id
        "?, " +  # fk_course_id
        "?, " +  # org_code
        "?, " +  # faculty_code
        "?, " +  # institute_code_a
        "?, " +  # institute_code_b
        "?, " +  # semester_code_1
        "?, " +  # semester_code_2
        "?, " +  # semester_code_3
        "?, " +  # term_code_1
        "?, " +  # term_name_1
        "?, " +  # term_code_2
        "?, " +  # term_name_2
        "?, " +  # term_code_3
        "?, " +  # term_name_3
        "?")  # year
    # //@formatter:on


def get_course_counter_initial_values(organisation):
    """Get initial values for counters."""
    return organisation, "0"


def get_course_values(organisation, course):
    """Get list of values applicable for a course insertion."""
    if 'created_date' in course:
        course['created_date'] = date_remove_z(course['created_date'])
    if 'last_modified_date' in course:
        course['last_modified_date'] = date_remove_z(course['last_modified_date'])
    if 'start_date' in course:
        course['start_date'] = date_remove_z(course['start_date'])
    if 'end_date' in course:
        course['end_date'] = date_remove_z(course['end_date'])

    term_code_1 = ''
    term_name_1 = ''
    term_code_2 = ''
    term_name_2 = ''
    term_code_3 = ''
    term_name_3 = ''

    if 'term' in course:
        terms = course['term']
        if len(terms) > 2:
            if 'value' in course['term'][2]:
                if 'value' in course['term'][2]:
                    term_code_1 = course['term'][2]['value']
                    term_name_1 = course['term'][2]['desc']
                if 'value' in course['term'][1]:
                    term_code_2 = course['term'][1]['value']
                    term_name_2 = course['term'][1]['desc']
                if 'value' in course['term'][0]:
                    term_code_3 = course['term'][0]['value']
                    term_name_3 = course['term'][0]['desc']
        elif len(terms) == 2:
                if 'value' in course['term'][1]:
                    term_code_2 = course['term'][1]['value']
                    term_name_2 = course['term'][1]['desc']
                if 'value' in course['term'][0]:
                    term_code_3 = course['term'][0]['value']
                    term_name_3 = course['term'][0]['desc']
        elif len(terms) == 1:
            if 'value' in course['term'][0]:
                term_code_3 = course['term'][0]['value']
                term_name_3 = course['term'][0]['desc']

    course_name = get_string_value_dict(course, 'name')
    course_name = repr(course_name.replace('\xc2\x96', '-'))
    # Some courses start and end with a single quote (ntnu issue)
    course_name = course_name.lstrip("'")
    course_name = course_name.rstrip("'")

    return (organisation,
            get_string_value_dict(course, 'id'),
            get_string_value_dict(course, 'code'),
            course_name,
            get_string_value_dict(course, 'section'),
            get_string_value_sub_dict(course, 'academic_department', 'value'),
            get_string_value_sub_dict(course, 'academic_department', 'desc'),
            term_code_1,
            term_name_1,
            term_code_2,
            term_name_2,
            term_code_3,
            term_name_3,
            get_string_value_dict(course, 'start_date'),
            get_string_value_dict(course, 'end_date'),
            get_numeric_value_dict(course, 'weekly_hours'),
            get_numeric_value_dict(course, 'participants'),
            get_string_value_dict(course, 'year'),
            get_string_value_dict(course, 'created_date'),
            get_string_value_dict(course, 'last_modified_date'),
            get_string_value_dict(course, 'rolled_from'),
            get_string_value_dict(course, 'course_leganto_code'),
            get_string_value_dict(course, 'org_code'),
            get_string_value_dict(course, 'faculty_code'),
            get_string_value_dict(course, 'institute_code_a'),
            get_string_value_dict(course, 'institute_code_b'),
            get_string_value_dict(course, 'status'))


def get_course_searchable_id_values(organisation, course_id, searchable_id):
    """Get list of values applicable for a searchable id insertion."""
    return organisation, course_id, searchable_id


def get_course_term_values(organisation, course_id, term):
    """Get list of values applicable for a term insertion."""
    return (organisation, course_id, get_string_value_dict(term, 'value'),
            get_string_value_dict(term, 'desc'))


def get_reading_list_note_values(organisation, reading_list_id, note):
    """Get list of values applicable for a note / reading list insertion."""
    return (organisation, reading_list_id, get_string_value_dict(note, 'content'),
            get_string_value_dict(note, 'creation_date'),
            get_string_value_dict(note, 'type'), get_checksum_note(note))


def get_course_reading_list_values(organisation, course_id, reading_list):
    """Get list of values applicable for a reading list / course insertion."""
    return (organisation,
            get_string_value_dict(reading_list, 'id'),
            get_string_value_dict(reading_list, 'code'),
            get_string_value_dict(reading_list, 'name'),
            get_string_value_dict(reading_list, 'order'),
            get_string_value_dict(reading_list, 'description'),
            get_string_value_dict(reading_list, 'last_modified_date'),
            get_string_value_sub_dict(reading_list, 'status', 'value'),
            get_string_value_sub_dict(reading_list, 'status', 'desc'),
            get_string_value_sub_dict(reading_list, 'visibility', 'value'),
            get_string_value_sub_dict(reading_list, 'visibility', 'desc'),
            get_string_value_sub_dict(reading_list, 'publishingStatus', 'value'),
            get_string_value_sub_dict(reading_list, 'publishingStatus', 'desc'),
            get_string_value_dict(reading_list, 'term_code_1'),
            get_string_value_dict(reading_list, 'term_name_1'),
            get_string_value_dict(reading_list, 'term_code_2'),
            get_string_value_dict(reading_list, 'term_name_2'),
            get_string_value_dict(reading_list, 'term_code_3'),
            get_string_value_dict(reading_list, 'term_name_4'),
            get_string_value_dict(reading_list, 'year'),
            course_id,
            get_boolean_value_dict(reading_list, 'blocked'))


def get_course_citation_values(organisation, course, reading_list_id, citation):
    """Get list of values applicable for a citation / reading list / course insertion."""

    term_code_1, term_name_1, term_code_2, term_name_2, term_code_3, term_name_3, semester_code_1, \
    semester_code_2, semester_code_3, department_name, department_code, org_code, faculty_code, \
    institute_code_a, institute_code_b = get_citations_values(course)

    citation['last_modified_date'] = date_remove_z(get_string_value_dict(citation,
                                                                         'last_modified_date'))
    return (organisation,
            get_string_value_dict(citation, 'id'),
            get_string_value_sub_dict(citation, 'status', 'value'),
            get_string_value_sub_dict(citation, 'status', 'desc'),
            get_string_value_sub_dict(citation, 'copyrights_status', 'value'),
            get_string_value_sub_dict(citation, 'copyrights_status', 'desc'),
            get_string_value_sub_dict(citation, 'secondary_type', 'value'),
            get_string_value_sub_dict(citation, 'secondary_type', 'desc'),
            get_string_value_sub_dict(citation, 'type', 'value'),
            get_string_value_sub_dict(citation, 'type', 'desc'),
            get_string_value_sub_dict(citation, 'metadata', 'title'),
            get_string_value_sub_dict(citation, 'metadata', 'author'),
            get_string_value_sub_dict(citation, 'metadata', 'publisher'),
            get_string_value_sub_dict(citation, 'metadata', 'publication_date'),
            get_string_value_sub_dict(citation, 'metadata', 'edition'),
            get_string_value_sub_dict(citation, 'metadata', 'isbn'),
            get_string_value_sub_dict(citation, 'metadata', 'issn'),
            get_string_value_sub_dict(citation, 'metadata', 'mms_id'),
            get_string_value_sub_dict(citation, 'metadata', 'additional_person_name'),
            get_string_value_sub_dict(citation, 'metadata', 'place_of_publication'),
            get_string_value_sub_dict(citation, 'metadata', 'call_number'),
            get_string_value_sub_dict(citation, 'metadata', 'note'),
            get_string_value_sub_dict(citation, 'metadata', 'journal_title'),
            get_string_value_sub_dict(citation, 'metadata', 'article_title'),
            get_string_value_sub_dict(citation, 'metadata', 'issue'),
            get_string_value_sub_dict(citation, 'metadata', 'editor'),
            get_string_value_sub_dict(citation, 'metadata', 'chapter'),
            get_string_value_sub_dict(citation, 'metadata', 'chapter_title'),
            get_string_value_sub_dict(citation, 'metadata', 'chapter_author'),
            get_string_value_sub_dict(citation, 'metadata', 'year'),
            get_string_value_sub_dict(citation, 'metadata', 'pages'),
            get_string_value_sub_dict(citation, 'metadata', 'source'),
            get_string_value_sub_dict(citation, 'metadata', 'series_title_number'),
            get_string_value_sub_dict(citation, 'metadata', 'pmid'),
            get_string_value_sub_dict(citation, 'metadata', 'doi'),
            get_string_value_sub_dict(citation, 'metadata', 'volume'),
            get_string_value_sub_dict(citation, 'metadata', 'start_page'),
            get_string_value_sub_dict(citation, 'metadata', 'end_page'),
            get_string_value_sub_dict(citation, 'metadata', 'start_page2'),
            get_string_value_sub_dict(citation, 'metadata', 'end_page2'),
            get_string_value_sub_dict(citation, 'metadata', 'author_initials'),
            get_string_value_sub_dict(citation, 'metadata', 'part'),
            get_string_value_dict(citation, 'public_note'),
            get_string_value_dict(citation, 'source1'),
            get_string_value_dict(citation, 'source2'),
            get_string_value_dict(citation, 'source3'),
            get_string_value_dict(citation, 'source4'),
            get_string_value_dict(citation, 'source5'),
            get_string_value_dict(citation, 'source6'),
            get_string_value_dict(citation, 'source7'),
            get_string_value_dict(citation, 'source8'),
            get_string_value_dict(citation, 'source9'),
            get_string_value_dict(citation, 'source10'),
            get_string_value_dict(citation, 'last_modified_date'),
            get_string_value_sub_dict(citation, 'section_info', 'id'),
            get_string_value_sub_dict(citation, 'section_info', 'name'),
            get_string_value_sub_dict(citation, 'section_info', 'description'),
            get_boolean_value_sub_dict(citation, 'section_info', 'visibility'),
            get_string_value_sub_sub_dict(
                citation, 'section_info', 'section_tags', 'total_record_count'),
            get_string_value_sub_sub_dict(citation, 'section_info', 'section_tags', 'link'),
            get_boolean_value_sub_dict(citation, 'section_info', 'section_locked'),
            get_string_value_dict(citation, 'file_link'),
            reading_list_id,
            course['id'],
            org_code,
            faculty_code,
            institute_code_a,
            institute_code_b,
            semester_code_1,
            semester_code_2,
            semester_code_3,
            term_code_1,
            term_name_1,
            term_code_2,
            term_name_2,
            term_code_3,
            term_name_3,
            course['year'])


def escape(value):
    """Escape a given value. Will need more work as more examples found"""
    value = value.replace("[", "\\[")
    return value.replace("'", r"\'")


def date_remove_z(value):
    """Remove Z, 'Zulu' from date as mysql can't handle it"""
    if value is not None and value[-1] == "Z":
        return value[:-1]
    return value


def check_for_unknown_course_keys(course):
    course_keys = list(course)
    for key in course_keys:
        if key not in known_course_keys:
            print('Found unknown key while processing a course [' + key + ']')
    if 'academic_department' in course:
        academic_department_keys = list(course['academic_department'])
        for key in academic_department_keys:
            if key not in known_course_academic_department_keys:
                print('Found unknown key while processing a course:academic_department[' +
                      key + ']')


def check_for_unknown_reading_list_keys(reading_list):
    reading_list_keys = list(reading_list)
    for key in reading_list_keys:
        if key not in known_reading_list_keys:
            print('Found unknown key while processing a reading_list [' + key + ']')
    if 'status' in reading_list:
        status_keys = list(reading_list['status'])
        for key in status_keys:
            if key not in known_reading_list_status_keys:
                print('Found unknown key while processing a reading_list:status[' + key + ']')
    if 'syllabus' in reading_list:
        syllabus_keys = list(reading_list['syllabus'])
        for key in syllabus_keys:
            if key not in known_reading_list_syllabus_keys:
                print('Found unknown key while processing a reading_list:syllabus[' + key + ']')
    if 'visibility' in reading_list:
        visibility_keys = list(reading_list['visibility'])
        for key in visibility_keys:
            if key not in known_reading_list_visibility_keys:
                print('Found unknown key while processing a reading_list:visibility[' + key + ']')
    if 'publishingStatus' in reading_list:
        publishing_status_keys = reading_list['publishingStatus']
        for key in publishing_status_keys:
            if key not in known_reading_list_publishing_status_keys:
                print('Found unknown key while processing a reading_list:publishing_status[' +
                      key + ']')


def check_for_unknown_citation_keys(citation):
    citation_keys = list(known_citation_keys)
    for key in citation_keys:
        if key not in known_citation_keys:
            print('Found unknown key while processing a citation [' + key + ']')
    if 'status' in citation:
        status_keys = list(citation['status'])
        for key in status_keys:
            if key not in known_citation_status_keys:
                print('Found unknown key while processing a citation:status[' + key + ']')
    if 'copyrights_status' in citation:
        copyrights_status_keys = list(citation['copyrights_status'])
        for key in copyrights_status_keys:
            if key not in known_citation_copyrights_status_keys:
                print('Found unknown key while processing a citation:copyrights_status[' +
                      key + ']')
    if 'type' in citation:
        type_keys = list(citation['type'])
        for key in type_keys:
            if key not in known_citation_type_keys:
                print('Found unknown key while processing a citation:type[' + key + ']')
    if 'metadata' in citation:
        metadata_keys = citation['metadata']
        for key in metadata_keys:
            if key not in known_citation_metadata_keys:
                print('Found unknown key while processing a citation:metadata[' +
                      key + ']')
    if 'section_info' in citation:
        section_info_keys = citation['section_info']
        for key in section_info_keys:
            if key not in known_citation_section_info_keys:
                print('Found unknown key while processing a citation:section_info[' +
                      key + ']')


def get_csv_course_column_names():
    """Get list of column names applicable for a csv description of course headers ."""
    return ["organisation", "local_id", "course_id", "course_code", "course_name", "section",
            "academic_department_code", "academic_department_name", "start_date", "end_date",
            "weekly_hours", "participants", "year", "created_date", "last_modified_date",
            "rolled_from", "course_leganto_code", "org_code", "faculty_code", "institute_code_a",
            "institute_code_b", "status"]


def get_csv_reading_list_column_names():
    """Get list of column names applicable for a csv description of reading_list headers ."""
    return ["organisation", "reading_list_id", "code", "name", "reading_list_order",
            "description", "last_modified_date", "status_value", "status_desc",
            "visibility_value", "visibility_desc", "publishing_status_value",
            "publishing_status_desc", "blocked", "fk_course_id"]


def get_csv_citation_column_names():
    """Get list of column names applicable for a csv description of citation headers ."""
    return ["organisation", 'code', 'course_name', 'org_code', 'faculty_code', 'institute_code_a',
            'institute_code_b', 'semester_year', 'semester_1', 'semester_2', 'semester_3',
            'semester_code_1', 'semester_code_2', 'semester_code_3', 'department_name',
            'department_code', "citation_id", "status_value", "status_desc",
            "copyrights_status_value", "copyrights_status_desc", "secondary_type_value",
            "secondary_type_desc", "type_value", "type_desc", "metadata_title",
            "metadata_author", "metadata_publisher", "metadata_publication_date",
            "metadata_edition", "metadata_isbn", "metadata_issn", "metadata_mms_id",
            "metadata_additional_person_name", "metadata_place_of_publication",
            "metadata_call_number", "metadata_note", "metadata_journal_title",
            "metadata_article_title", "metadata_issue", "metadata_editor", "metadata_chapter",
            "metadata_chapter_title", "metadata_chapter_author", "metadata_year",
            "metadata_pages", "metadata_source", "metadata_series_title_number", "metadata_pmid",
            "metadata_doi", "metadata_volume", "metadata_start_page", "metadata_end_page",
            "metadata_start_page2", "metadata_end_page2", "metadata_author_initials",
            "metadata_part", "public_note", "source1", "source2",
            "source3", "source4", "source5", "source6", "source7", "source8", "source9",
            "source10", "last_modified_date", "section_info_id", "section_info_name",
            "section_info_description", "section_info_visibility",
            "section_info_tags_total_rec_cnt", "section_info_section_tags_link",
            "section_info_section_locked", "file_link", "fk_reading_list_id",
            "fk_course_id", "course_leganto_code"]


def get_csv_values_course(organisation, course, counter):
    return {'organisation': organisation,
            'local_id': counter,
            'course_id': get_string_value_dict(course, 'id'),
            'course_code': get_string_value_dict(course, 'code'),
            'course_name': get_string_value_dict(course, 'name'),
            'section': get_string_value_dict(course, 'section'),
            'academic_department_code': get_string_value_sub_dict(course, 'academic_department',
                                                                  'value'),
            'academic_department_name': get_string_value_sub_dict(course, 'academic_department',
                                                                  'desc'),
            'start_date': date_remove_z(get_string_value_dict(course, 'start_date')),
            'end_date': date_remove_z(get_string_value_dict(course, 'end_date')),
            'weekly_hours': get_numeric_value_dict(course, 'weekly_hours'),
            'participants': get_numeric_value_dict(course, 'participants'),
            'year': get_string_value_dict(course, 'year'),
            'created_date': date_remove_z(get_string_value_dict(course, 'created_date')),
            'last_modified_date': date_remove_z(get_string_value_dict(course,
                                                                      'last_modified_date')),
            'rolled_from': get_string_value_dict(course, 'rolled_from'),
            'course_leganto_code': get_string_value_dict(course, 'course_leganto_code'),
            'org_code': get_string_value_dict(course, 'org_code'),
            'faculty_code': get_string_value_dict(course, 'faculty_code'),
            'institute_code_a': get_string_value_dict(course, 'institute_code_a'),
            'institute_code_b': get_string_value_dict(course, 'institute_code_b'),
            'status': get_string_value_dict(course, 'status')
            }


def get_csv_values_reading_list(organisation, course_id, reading_list):
    return {'organisation': organisation,
            'reading_list_id': get_string_value_dict(reading_list, 'id'),
            'code': get_string_value_dict(reading_list, 'code'),
            'name': get_string_value_dict(reading_list, 'name'),
            'reading_list_order': get_string_value_dict(reading_list, 'order'),
            'description': get_string_value_dict(reading_list, 'description'),
            'last_modified_date': date_remove_z(get_numeric_value_dict(reading_list,
                                                                       'last_modified_date')),
            'status_value': get_string_value_sub_dict(reading_list, 'status', 'value'),
            'status_desc': get_string_value_sub_dict(reading_list, 'status', 'desc'),
            'visibility_value': get_string_value_sub_dict(reading_list, 'visibility', 'value'),
            'visibility_desc': get_string_value_sub_dict(reading_list, 'visibility', 'desc'),
            'publishing_status_value': get_string_value_sub_dict(reading_list, 'publishingStatus',
                                                                 'value'),
            'publishing_status_desc': get_string_value_sub_dict(reading_list, 'publishingStatus',
                                                                'desc'),
            'blocked': get_boolean_value_dict(reading_list, 'blocked'),
            'fk_course_id': course_id}


def get_citations_values(course):
    term_code_1 = ''
    term_name_1 = ''
    term_code_2 = ''
    term_name_2 = ''
    term_code_3 = ''
    term_name_3 = ''
    semester_code_1 = ''
    semester_code_2 = ''
    semester_code_3 = ''

    if 'term' in course.keys():
        if len(course['term']) == 0:
            pass
        elif len(course['term']) == 1:
            if 'value' in course['term'][0]:
                term_code_1 = course['term'][0]['value']
                term_name_1 = course['term'][0]['desc']
                semester_code_1 = course['year'] + "-" + term_code_1
        elif len(course['term']) == 2:
            if 'value' in course['term'][1]:
                term_code_1 = course['term'][0]['value']
                term_name_1 = course['term'][0]['desc']
                semester_code_1 = course['year'] + "-" + term_code_1
                term_code_2 = course['term'][1]['value']
                term_name_2 = course['term'][1]['desc']
                semester_code_2 = course['year'] + "-" + term_code_2
        elif len(course['term']) == 3:
            if 'value' in course['term'][2]:
                term_code_1 = course['term'][0]['value']
                term_name_1 = course['term'][0]['desc']
                semester_code_1 = course['year'] + "-" + term_code_1
                term_code_2 = course['term'][1]['value']
                term_name_2 = course['term'][1]['desc']
                semester_code_2 = course['year'] + "-" + term_code_2
                term_code_3 = course['term'][2]['value']
                term_name_3 = course['term'][2]['desc']
                semester_code_3 = course['year'] + "-" + term_code_3
        else:
            print("Citations process. Multiple semesters. Unsure")
            raise "Multiple semesters. Unsure"

    department_name = ''
    department_code = ''
    if 'academic_department' in course.keys():
        department_name = course['academic_department']['desc']
        department_code = course['academic_department']['value']

    org_code = ''
    if 'org_code' in course.keys():
        org_code = course['org_code']

    faculty_code = ''
    if 'faculty_code' in course.keys():
        faculty_code = course['faculty_code']

    institute_code_a = ''
    if 'institute_code_a' in course.keys():
        institute_code_a = course['institute_code_a']

    institute_code_b = ''
    if 'institute_code_b' in course.keys():
        institute_code_b = course['institute_code_b']

    return term_code_1, term_name_1, term_code_2, term_name_2, term_code_3, term_name_3, \
           semester_code_1, semester_code_2, semester_code_3, department_name, department_code, \
           org_code, faculty_code, institute_code_a, institute_code_b


def get_csv_values_citation(organisation, course, reading_list_id, citation):
    term_code_1, term_name_1, term_code_2, term_name_2, term_code_3, term_name_3, \
    semester_code_1, semester_code_2, semester_code_3, department_name, department_code, \
    org_code, faculty_code, institute_code_a, institute_code_b = get_citations_values(course)

    return {'organisation': organisation,
            'code': course['code'],
            'course_name': course['name'],
            'org_code': org_code,
            'faculty_code': faculty_code,
            'institute_code_a': institute_code_a,
            'institute_code_b': institute_code_b,
            'semester_year': course['year'],
            'semester_1': term_code_1,
            'semester_2': term_code_2,
            'semester_3': term_code_3,
            'semester_code_1': semester_code_1,
            'semester_code_2': semester_code_2,
            'semester_code_3': semester_code_3,
            'department_name': department_name,
            'department_code': department_code,
            'citation_id': get_string_value_dict(citation, 'id'),
            'status_value': get_string_value_sub_dict(citation, 'status', 'value'),
            'status_desc': get_string_value_sub_dict(citation, 'status', 'desc'),
            'copyrights_status_value': get_string_value_sub_dict(citation, 'copyright_status',
                                                                 'value'),
            'copyrights_status_desc': get_string_value_sub_dict(citation, 'copyright_status',
                                                                'desc'),
            'secondary_type_value': get_string_value_sub_dict(citation, 'secondary_type', 'value'),
            'secondary_type_desc': get_string_value_sub_dict(citation, 'secondary_type', 'desc'),
            'type_value': get_string_value_sub_dict(citation, 'type', 'value'),
            'type_desc': get_string_value_sub_dict(citation, 'type', 'desc'),
            'metadata_title': get_string_value_sub_dict(citation, 'metadata', 'title'),
            'metadata_author': get_string_value_sub_dict(citation, 'metadata', 'author'),
            'metadata_publisher': get_string_value_sub_dict(citation, 'metadata', 'publisher'),
            'metadata_publication_date': get_string_value_sub_dict(citation, 'metadata',
                                                                   'publication_date'),
            'metadata_isbn': get_string_value_sub_dict(citation, 'metadata', 'isbn'),
            'metadata_issn': get_string_value_sub_dict(citation, 'metadata', 'issn'),
            'metadata_mms_id': get_string_value_sub_dict(citation, 'metadata', 'mms_id'),
            'metadata_additional_person_name': get_string_value_sub_dict(citation, 'metadata',
                                                                         'additional_person_name'),
            'metadata_place_of_publication': get_string_value_sub_dict(citation, 'metadata',
                                                                       'place_of_publication'),
            'metadata_call_number': get_string_value_sub_dict(citation, 'metadata', 'call_number'),
            'metadata_note': get_string_value_sub_dict(citation, 'metadata', 'note'),
            'metadata_journal_title': get_string_value_sub_dict(citation, 'metadata',
                                                                'journal_title'),
            'metadata_article_title': get_string_value_sub_dict(citation, 'metadata',
                                                                'article_title'),
            'metadata_issue': get_string_value_sub_dict(citation, 'metadata', 'issue'),
            'metadata_editor': get_string_value_sub_dict(citation, 'metadata', 'editor'),
            'metadata_chapter': get_string_value_sub_dict(citation, 'metadata', 'chapter'),
            'metadata_chapter_title': get_string_value_sub_dict(citation, 'metadata',
                                                                'chapter_title'),
            'metadata_chapter_author': get_string_value_sub_dict(citation, 'metadata',
                                                                 'chapter_author'),
            'metadata_year': get_string_value_sub_dict(citation, 'metadata', 'year'),
            'metadata_pages': get_string_value_sub_dict(citation, 'metadata', 'pages'),
            'metadata_source': get_string_value_sub_dict(citation, 'metadata', 'source'),
            'metadata_series_title_number': get_string_value_sub_dict(citation, 'metadata',
                                                                      'series_title_number'),
            'metadata_pmid': get_string_value_sub_dict(citation, 'metadata', 'pmid'),
            'metadata_doi': get_string_value_sub_dict(citation, 'metadata', 'doi'),
            'metadata_volume': get_string_value_sub_dict(citation, 'metadata', 'volume'),
            'metadata_start_page': get_string_value_sub_dict(citation, 'metadata', 'start_page'),
            'metadata_end_page': get_string_value_sub_dict(citation, 'metadata', 'end_page'),
            'metadata_start_page2': get_string_value_sub_dict(citation, 'metadata', 'start_page2'),
            'metadata_end_page2': get_string_value_sub_dict(citation, 'metadata', 'end_page2'),
            'metadata_author_initials': get_string_value_sub_dict(citation, 'metadata',
                                                                  'author_initials'),
            'metadata_part': get_string_value_sub_dict(citation, 'metadata', 'part'),
            'public_note': get_string_value_dict(citation, 'public_note'),
            'source1': get_string_value_dict(citation, 'source1'),
            'source2': get_string_value_dict(citation, 'source2'),
            'source3': get_string_value_dict(citation, 'source3'),
            'source4': get_string_value_dict(citation, 'source4'),
            'source5': get_string_value_dict(citation, 'source5'),
            'source6': get_string_value_dict(citation, 'source6'),
            'source7': get_string_value_dict(citation, 'source7'),
            'source8': get_string_value_dict(citation, 'source8'),
            'source9': get_string_value_dict(citation, 'source9'),
            'source10': get_string_value_dict(citation, 'source10'),
            'last_modified_date': date_remove_z(get_string_value_dict(citation,
                                                                      'last_modified_date')),
            'section_info_id': get_string_value_sub_dict(citation, 'section_info', 'id'),
            'section_info_name': get_string_value_sub_dict(citation, 'section_info', 'name'),
            'section_info_description': get_string_value_sub_dict(citation, 'section_info',
                                                                  'description'),
            'section_info_visibility': get_boolean_value_sub_dict(citation, 'section_info',
                                                                  'visibility'),
            'section_info_tags_total_rec_cnt': get_string_value_sub_sub_dict(citation,
                                                                             'section_info',
                                                                             'section_tags',
                                                                             'total_record_count'),
            'section_info_section_tags_link': get_string_value_sub_sub_dict(citation,
                                                                            'section_info',
                                                                            'section_tags',
                                                                            'link'),
            'section_info_section_locked': get_boolean_value_sub_dict(citation, 'section_info',
                                                                      'locked'),
            'file_link': get_string_value_dict(citation, 'file_link'),
            'fk_reading_list_id': reading_list_id,
            'fk_course_id': course['id'],
            'course_leganto_code': course['course_leganto_code']}


def is_digit(string_to_check):
    return string_to_check.replace('.', '', 1).isdigit()


def get_checksum_note(note):
    values = note['content'] + note['creation_date']
    checksum = sha256(values.encode()).hexdigest()
    return checksum


def from_value_get_name_uit(organisation, dep_value, dep_name):
    # UiT id's appear to double a 0
    # Note a single 0 is extracted later so here we remove if it ends with 0000 or 00000, the 00 is
    # taken care of later
    if dep_value.endswith('000000'):
        dep_value = dep_value[:len(dep_value) - 2]
    elif dep_value.endswith('0000'):
        dep_value = dep_value[:len(dep_value) - 1]
    # In some cases the value is missing a 0, so we add it
    elif dep_value == '186000':
        dep_value = '1860000'
    # The course with academic:department.value == 18633150, needs to get an additional 0 added
    # so it can be processed below
    elif dep_value == '18633150':
        dep_value = '186331500'

    # In some cases, courses are listed with a faculty id, but no name
    if dep_value.count('_') == 2:
        dep_value = dep_value.replace('_', '')
        dep_value = dep_value + '00'
    with open('./json/' + organisation + '_cristin.json') as org_file:
        data = json.load(org_file)
        for i in range(len(data)):
            cristin_unit = data[i]
            stripped_cristin_dep_value = cristin_unit['cristin_unit_id'].replace('.', '')
            # There is a en extra 0 on UiT data
            length = len(dep_value)
            cut_dep_value = dep_value[:length - 1]
            if cut_dep_value == stripped_cristin_dep_value:
                if isinstance(cristin_unit['unit_name'], dict):
                    return cristin_unit['cristin_unit_id'], cristin_unit['unit_name']['nb']
                else:
                    return cristin_unit['cristin_unit_id'], cristin_unit['unit_name']
        return dep_value, dep_name


def fix_dept_value(organisation, dep_value, dep_name):
    correct_dep_value = dep_value
    potential_dep_value = dep_value
    potential_dep_name = dep_name

    with open('./json/' + organisation + '_cristin_id_list.json') as org_file:
        data = json.load(org_file)
        for i in range(len(data)):
            val = data[i]
            if dep_name in val.keys():
                correct_dep_value = val[dep_name]['id']
                # Just do a check to make sure the actual dep_value is similar to the identified one
                # i.e. 186331400 is similar to 186.33.14.00. We remove the '.' and check that
                # strings are contained within each other
                stripped_dep_value = correct_dep_value.replace('.', '')

                if stripped_dep_value not in dep_value:
                    potential_dep_value = val[dep_name]['id']
                    potential_dep_name = dep_name
                    print(
                        "\tFound potential match [{}] and [{}]".format(dep_value,
                                                                       correct_dep_value))
                else:
                    correct_dep_value = val[dep_name]['id']
                    print(
                        "\tFound actual match [{}] and [{}]".format(dep_value, correct_dep_value))

        # We did not find an actual match, check the potential match
        if correct_dep_value == dep_value:
            # We have a potential map
            if potential_dep_value != dep_value:
                correct_dep_value = potential_dep_value

        return correct_dep_value, potential_dep_name


def semester_to_english(semester_norwegian):
    if semester_norwegian is None:
        return ""
    elif 'høst' in semester_norwegian.lower():
        return 'autumn'
    elif 'vår' in semester_norwegian.lower():
        return 'spring'
    elif 'sommer' in semester_norwegian.lower():
        return 'summer'
    # The next three are to make sure that a semester always has a lower case
    elif 'autumn' in semester_norwegian.lower():
        return 'autumn'
    elif 'spring' in semester_norwegian.lower():
        return 'spring'
    elif 'summer' in semester_norwegian.lower():
        return 'summer'
    else:
        return 'unknown semester ' + semester_norwegian


def to_norwegian(semester_english):
    if semester_english is None:
        return ""
    elif 'autumn' in semester_english.lower():
        return 'høst'
    elif 'spring' in semester_english.lower():
        return 'vår'
    elif 'summer' in semester_english.lower():
        return 'sommer'
    else:
        return 'unknown semester ' + semester_english


# Lists of known keys in JSON payloads. Used so we can check if the API returns additional
# attributes that we are not aware of.
known_course_keys = ['id', 'code', 'name', 'section', 'status', 'start_date', 'end_date',
                     'weekly_hours', 'participants', 'year', 'created_by', 'created_date',
                     'last_modified_by', 'last_modified_date', 'rolled_from', 'link',
                     'academic_department', 'processing_department', 'term', 'instructor', 'campus',
                     'searchable_id', 'note', 'academic_department', 'course_leganto_code',
                     'org_code', 'faculty_code', 'institute_code_a', 'institute_code_b']
known_course_academic_department_keys = ['value', 'desc']
known_course_processing_department_keys = ['value', 'desc']
known_reading_list_keys = ['id', 'code', 'name', 'due_back_date', 'order', 'note', 'description',
                           'locked', 'last_modified_date', 'link', 'publishingStatus', 'syllabus',
                           'status', 'visibility', 'notes', 'citations', 'blocked', 'term_code_1',
                           'term_name_1', 'term_code_2', 'term_name_2', 'term_code_3',
                           'term_name_3', 'year']
known_reading_list_status_keys = ['value', 'desc']
known_reading_list_syllabus_keys = ['url', 'file']
known_reading_list_visibility_keys = ['value', 'desc']
known_reading_list_publishing_status_keys = ['value', 'desc']
known_citation_keys = ['id', 'open_url', 'leganto_permalink', 'public_note', 'source1', 'source2',
                       'source3', 'source4', 'source5', 'source6', 'source7', 'source8', 'source9',
                       'source10', 'last_modified_date', 'link', 'file_link']
known_citation_status_keys = ['value', 'desc']
known_citation_copyrights_status_keys = ['value', 'desc']
known_citation_type_keys = ['value', 'desc']
known_citation_metadata_keys = ['title', 'author', 'publisher', 'publication_date', 'edition',
                                'isbn', 'issn', 'mms_id', 'additional_person_name',
                                'place_of_publication', 'note', 'issue', 'editor', 'chapter',
                                'year',
                                'pages', 'source', 'pmid', 'doi', 'volume', 'part', 'call_number',
                                'journal_title', 'article_title', 'chapter_title', 'chapter_author',
                                'series_title_number', 'start_page', 'end_page', 'start_page2',
                                'end_page2', "author_initials"]
known_citation_section_info_keys = ['id', 'name', 'description', 'visibility', 'section_locked',
                                    'section_tags', 'start_date', 'end_date']

org_information = [
    {'aho': {'id': '189', 'name': ' Arkitektur- og designhøgskolen'}},
    {'dmmh': {'id': '253', 'name': 'Dronning Mauds Minne Høgskole for barnehageutdanning'}},
    {'him': {'id': '211', 'name': 'Høgskolen i Molde'}},
    {'hio': {'id': '224', 'name': 'Høgskolen i Østfold'}},
    {'hk': {'id': '1615', 'name': 'Høyskolen Kristiania'}},
    {'hvl': {'id': '203', 'name': 'Høgskulen på Vestlandet'}},
    {'hvo': {'id': '223', 'name': 'Høgskulen i Volda'}},
    {'inn': {'id': '209', 'name': 'Høgskolen i innlandet'}},
    {'lovishs': {'id': '230', 'name': 'Lovisenberg diakonale høgskole'}},
    {'mf': {'id': '190', 'name': 'MF vitenskapelig høyskole'}},
    {'nhh': {'id': '191', 'name': 'Norges handelshøyskole'}},
    {'nla': {'id': '254', 'name': 'NLA Høgskolen'}},
    {'nmbu': {'id': '192', 'name': 'Norges miljø- og biovitenskapelige universitet'}},
    {'nord': {'id': '204', 'name': 'Nord universitet'}},
    {'ntnu': {'id': '194', 'name': 'NTNU'}},
    {'oslomet': {'id': '215', 'name': 'OsloMet'}},
    {'uia': {'id': '201', 'name': 'Universitetet i Agder'}},
    {'uib': {'id': '184', 'name': 'Universitetet i Bergen'}},
    {'uio': {'id': '185', 'name': 'Universitetet i Oslo'}},
    {'uis': {'id': '217', 'name': 'Universitetet i Stavanger'}},
    {'uit': {'id': '186', 'name': 'Universitetet i Tromsø'}},
    {'usn': {'id': '222', 'name': 'Universitetet i Sørøst-Norge'}},
    {'vid': {'id': '251', 'name': 'VID vitenskapelige høgskole'}}]
