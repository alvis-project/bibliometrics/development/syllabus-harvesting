from constant import get_string_value_sub_dict
from utility.leganto_constants import fix_dept_value, org_information, from_value_get_name_uit


def process_department_values(organisation, course):
    dep_value = get_string_value_sub_dict(course, 'academic_department', 'value')
    dep_name = get_string_value_sub_dict(course, 'academic_department', 'desc')

    # The values in the following list should be ignored as they have no proper organisation
    # counterpart.
    #
    # 'undefined', 'leganto' is a common identifier that can be swapped out with organisation
    # '1' is from aho. Some courses have a processing department == 1
    # 'geografi', 'infomedia', 'sampol', "ahkr", 'econ' are from UiT. Maybe early definition of
    # organisations
    # acdpt' comes from hvo
    # 'his' comes from inn
    # dmmhcoursereserves from dmmh
    known_words_to_ignore = ['acdpt', 'his', "ahkr", 'econ', 'geografi', 'infomedia', 'sampol',
                             'undefined', 'leganto', '1', 'dmmhcoursereserves']
    if organisation == 'uit':
        dep_value, dep_name = process_uit_org_values(dep_value, dep_name)

    if organisation == 'uib':
        dep_value, dep_name = process_uib_org_values(dep_value, dep_name)

    if dep_value is not None and dep_value.lower() == 'undefined':
        course['academic_department']['value'] = find_org_value_information(organisation)
        course['academic_department']['desc'] = find_org_desc_information(organisation)

    academic_dep_object = course['academic_department']

    if organisation == 'uit' and 'desc' not in academic_dep_object.keys():
        course['academic_department']['desc'] = ''
        course['academic_department']['value'], course['academic_department']['desc'] = \
            from_value_get_name_uit(organisation, dep_value, dep_name)
        dep_value = course['academic_department']['value']
        dep_name = course['academic_department']['desc']
    elif organisation == 'uit' or organisation == 'uib':
        correct_dep_value, potential_dep_name = fix_dept_value(organisation, dep_value, dep_name)
        course['academic_department']['value'] = correct_dep_value
        if 'desc' not in course['academic_department']:
            course['academic_department']['desc'] = potential_dep_name
        return

    if dep_value is not None and dep_value.isnumeric():
        course['academic_department']['value'] = str(dep_value)
    elif dep_value is not None and dep_value.lower() not in known_words_to_ignore and '_' not in \
            dep_value and '.' not in dep_value:
        correct_dep_value, potential_dep_name = fix_dept_value(organisation, dep_value, dep_name)
        course['academic_department']['value'] = correct_dep_value
        if course['academic_department']['desc'] is None:
            course['academic_department']['desc'] = potential_dep_name
    elif dep_value is not None and '_' in dep_value:
        course['academic_department']['value'] = dep_value.replace("_", ".")
    else:
        # True when no information is available
        # e.g., "academic_department": {},
        course['academic_department']['value'] = find_org_value_information(organisation)
        course['academic_department']['desc'] = find_org_desc_information(organisation)


def process_uib_org_values(dep_value, dep_name):
    if dep_value is not None:
        if dep_value.lower() == 'infomedia':
            dep_name = 'Institutt for informasjons- og medievitenskap'
        elif dep_value.lower() == 'geografi':
            dep_name = 'Institutt for geografi'
        elif dep_value.lower() == 'sampol':
            dep_name = 'Institutt for sammenliknende politikk'
        elif dep_value.lower() == 'econ':
            dep_name = 'Department of Economics'
        elif dep_value.lower() == 'ahkr':
            dep_name = 'Institutt for arkeologi, historie, kultur- og religionsvitenskap'
    return dep_value, dep_name


def process_uit_org_values(dep_value, dep_name):
    if dep_value is None:
        dep_value = '186000'
    if dep_value == '186':
        dep_value = '186000'
    if dep_name is None:
        dep_name = 'Universitetet i Tromsø - Norges arktiske universitet'
    return dep_value, dep_name


def find_org_value_information(organisation):
    for organisation_idx in org_information:
        if organisation in organisation_idx.keys():
            return organisation_idx[organisation]['id']
    raise 'Cannot continue as [' + organisation + '] is unknown'


def find_org_desc_information(organisation):
    for organisation_idx in org_information:
        if organisation in organisation_idx.keys():
            return organisation_idx[organisation]['name']
    raise 'Cannot continue as [' + organisation + '] is unknown'
