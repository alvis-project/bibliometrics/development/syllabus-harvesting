#!/bin/bash

organisation_list=(aho  dmmh  him  hio  hk  hvl  hvo  inn  lovishs  mf  nhh  nla  nmbu  nord  ntnu  uia  uib  uio  uis  uit  usn  vi)
what_list=(courses reading_lists citations)

for organisation in "${organisation_list[@]}"; do
  for what in "${what_list[@]}"; do
    python3 transform.py $what organisation=$organisation format=db
  done
done