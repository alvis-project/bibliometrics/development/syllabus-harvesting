# List of leganto institutions and their code

## List of known working Leganto institutions

 - Arkitektur- og designhøgskolen i Oslo = aho
 - Dronning Mauds Minne Høgskole = dmmh
 - Forsvarets høgskole = forsvaret
 - Høgskolen i Molde = himolde
 - Høgskulen i Volda = hivolda
 - Høgskolen i Østfold = hiof
 - Høgskulen på Vestlandet = hvl
 - Høgskolen i Innlandet = inn
 - Høyskolen Kristiania = kristiania
 - Kriminalomsorgens høgskole- og utdanningssenter (KRUS) = krus
 - Lovisenberg diakonale høgskole = ldh
 - MF vitenskapelig høyskole = mf
 - NLA Høgskolen = nla
 - Norges Handelshøyskole = nhh
 - Norges miljø- og biovitenskaplige universitet = nmbu 
 - Nord universitet = nord
 - NTNU = ntnu
 - OsloMet = oslomet
 - Politihøgskolen = phs
 - Sámi allaskuvla/Samisk høgskole = samas
 - Universitet i Agder = uia
 - Universitetet i Bergen = uib
 - Universitetet i Oslo = uio
 - Universitetet i Stavanger = uis
 - Universitetet i Tromsø = uit
 - Universitetet i Sørøst-Norge = usn
 - VID vitenskapelige høgskole = vid

The order of the above list is alphabetic, ascending based on the organisations code.

## List of Institutions by type 

The [source](https://no.wikipedia.org/wiki/Liste_over_universitet_og_h%C3%B8gskoler_i_Norge) is 
a list from Wikipedia. 

### Universities

- Norges miljø- og biovitenskaplige universitet 
- Nord universitet
- NTNU
- OsloMet
- Universitet i Agder
- Universitetet i Bergen
- Universitetet i Oslo
- Universitetet i Stavanger
- Universitetet i Tromsø
- Universitetet i Sørøst-Norge

Comment: Leganto covers 100% of universities in Norway.

### Scientific colleges
 - Arkitektur- og designhøgskolen i Oslo
 - _Handelshøyskolen BI_ (use tallis)
 - Høgskolen i Molde
 - _Kunsthøgskolen i Oslo_ (not in leganto consortium)
 - MF vitenskapelig høyskole
 - Norges Handelshøyskole 
 - _Norges idrettshøgskole_ (not in leganto consortium)
 - _Norges musikkhøgskole_ (not in leganto consortium)
 - VID vitenskapelige høgskole

### Colleges

#### State owned

 - Høgskolen i Innlandet 
 - Høgskulen i Volda 
 - Høgskolen i Østfold
 - Høgskulen på Vestlandet
 - Sámi allaskuvla/Samisk høgskole

#### Other state owned colleges

 - Politihøgskolen 
 - Forsvarets høgskole 

### Private colleges 

 - _Ansgar høyskole_ (not in leganto consortium)	
 - Dronning Mauds Minne Høgskole 	
 - Høyskolen Kristiania
 - Lovisenberg diakonale høgskole	
 - NLA Høgskolen
 
### Colleges with accredited study options
 - Kriminalomsorgens høgskole og utdanningssenter KRUS
 - _Atlantis_ (not in leganto consortium)
 - _Barratt Due musikkinstitutt_ (not in leganto consortium)
 - _Bergen arkitektskole (BAS)_ (not in leganto consortium)
 - _Høgskulen for Grøn Utvikling (HGUt)_ (not in leganto consortium)
 - _Høyskolen for dansekunst (HFDK)_ (not in leganto consortium)
 - _Høyskolen for Ledelse og Teologi_ (not in leganto consortium)
 - _Høyskolen for yrkesfag_ (not in leganto consortium)
 - _Lillehammer Institute of Music Production and Industries (LIMPI)_ (not in leganto consortium)	
 - _Noroff_ (not in leganto consortium)
 - _Norsk barnebokinstitutt_ (not in leganto consortium)
 - _Norsk Gestaltinstitutt_ (not in leganto consortium)
 - _NSKI Høyskole_ (not in leganto consortium)
 - _Oslo Nye Høyskole_ (not in leganto consortium)
 - _Steinerhøyskolen_ (not in leganto consortium)
 - _Skrivekunstakademiet_ (not in leganto consortium)
