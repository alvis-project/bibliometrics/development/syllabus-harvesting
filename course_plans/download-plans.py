import getopt
import json
import os
import sys
import time
from os.path import join

import requests
from bs4 import BeautifulSoup

from constant import ALL, PLANS
from db.sqlite_db import SQLiteDb

from selenium import webdriver

from utility.leganto_constants import to_norwegian

headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:84.0) Gecko/20100101 "
                  "Firefox/84.0"
}


class DownloadPlan:

    def __init__(self, organisation, db_file, working_dir):
        self.organisation = organisation
        self.db_file = db_file
        self.working_dir = working_dir
        self.org_keywords = {
            'oslomet': {"terms": "studieinfo emne",
                        "domain": "oslomet.no"}
        }
        # Check that plans directory exists, create otherwise
        self.create_directory_if_not_exists()

    def create_directory_if_not_exists(self):
        try:
            directory_name = join(self.working_dir, self.organisation, PLANS)
            if not os.path.exists(directory_name):
                os.makedirs(directory_name)
        except OSError:
            raise

    def check_url(self, href, organisation, course_code, year, semester, domain):
        if organisation not in href:
            print("Organisation [{}] not in href [{}]".format(organisation, href))
            return False
        elif course_code not in href:
            print("Course code [{}] not in href [{}]".format(course_code, href))
            return False
        elif year not in href:
            print("Year [{}] not in href [{}]".format(year, href))
            return False
        elif semester not in href:
            print("Semester [{}] not in href [{}]".format(semester, href))
            return False
        elif domain not in href:
            print("Domain [{}] not in href [{}]".format(domain, href))
            return False
        return True

    def download_study_plan(self, href, organisation, course_code, year, semester):
        response = requests.get(url=href, headers=headers)
        if response.status_code is not 200:
            print("href [{}] returned the following status code [{}]"
                  .format(href, str(response.status_code)))
            return

        file_name = organisation + '_' + course_code + '_' + str(year) + '_' + semester + '.html'
        file_path = join(self.working_dir, file_name)

        options = webdriver.ChromeOptions()
        #options.add_argument('--ignore-certificate-errors')
        #options.add_argument('--incognito')
        options.add_argument('--headless')
        driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver", chrome_options=options)
        driver.get(href)
        #el1 = driver.find_elements_by_class_name('')
        page_source = driver.page_source
        soup = BeautifulSoup(page_source, 'lxml')
        # Wait for the page to fully load
        driver.implicitly_wait(4)
        reviews = []
        panels = soup.find_all('div', class_='panel')
        reviews_selector1 = soup.find_all('div', class_='panel-heading')
        reviews_selector2 = soup.find_parent('div', class_='panel-heading')

        course_data = {}

        for panel in panels:
            # Get panel header
            header = panel.find('h2')
            header_content = str(header.text).strip()
            print(header_content)
            course_data[header_content] = ''
            # Get panel contents
            content = panel.find_next(class_="panel-body")
            f = content.format()
            for x in content.contents:
                print("[" + x.text + "]")

            if content is not None:
                panel_content = str(content.text).strip()
                print(panel_content)
                course_data[header_content] = panel_content

        print(json.dumps(course_data, indent=4))

        with open(file_path, 'w') as plan_file:
            plan_file.write(response.text)
            plan_file.flush()
            plan_file.close()
        time.sleep(3)

    def process_oslomet(self):
        pass


    def process(self):
        syllabus_db = SQLiteDb(self.db_file, self.organisation)
        course_total = syllabus_db.get_course_counter()
        limit = 20

        for offset in range(0, course_total, limit):
            print("Getting " + str(offset) + " " + str(limit) + " courses")
            courses = syllabus_db.get_courses(offset, limit)
            for course_as_tuple in courses:
                search_url = "https://html.duckduckgo.com/html/"
                course_code = course_as_tuple['course_code'].lower()
                year = course_as_tuple['year']

                semester = to_norwegian(course_as_tuple['term_code'].lower())
                domain = self.org_keywords[self.organisation]['domain']
                terms = self.org_keywords[self.organisation]['terms']
                params_string = '" [{}]  [{}]  [{}]  [{}]" site: [{}]'.format(terms, course_code,
                                                                              year, semester,
                                                                              domain)
                params = {'q': params_string}
                print("Search url is [{}]".format(search_url))


                self.download_study_plan('https://student.oslomet.no/studier/-/studieinfo/emne/MBIB4140/2020/H%C3%98ST',
                                         'oslomet', "lig6000", '2020', 'høst')





                response = requests.get(url=search_url, headers=headers, params=params)

                soup = BeautifulSoup(response.text, 'lxml')
                result_urls = soup.find_all(class_='result__url')

                for result_url in result_urls:
                    href_unprocessed = str(result_url['href'])
                    href = requests.utils.unquote(href_unprocessed)
                    href = href.lower()
                    if self.check_url(href, self.organisation, course_code, year, semester, domain):
                        self.download_study_plan(href, self.organisation, course_code, year,
                                                 semester)


def main(argv):
    organisation_to_process, db_file, working_dir = get_options_retrieve(argv)

    if organisation_to_process == ALL:
        organisations = os.listdir(working_dir)
    else:
        organisations = [organisation_to_process]

    for organisation in organisations:
        downloader = DownloadPlan(organisation, db_file, working_dir)
        downloader.process()

    sys.exit(0)


def get_options_retrieve(argv):
    try:
        # Assume all organisations to be processed
        organisation = ALL
        organisation = 'oslomet'

        working_dir = '../leganto'
        # Assume syllabus.sqlite as default file
        db_file = '../syllabus.sqlite'
        opts, args = getopt.getopt(argv, 'hc:i:b:w', ['organisation=', 'db-file=', 'working-dir='])
        for opt, arg in opts:
            if opt == '-h':
                print('download-plans.py -i <organisation>  -b <database-file>')
                sys.exit()
            if opt in ('-i', '--organisation'):
                organisation = arg
            if opt in ('-b', '--database-file'):
                db_file = arg
            if opt in ('-d', '--working-dir'):
                working_dir = arg

        return organisation, db_file, working_dir

    except getopt.GetoptError:
        print('db-utils.py -c <command> -i <organisation>  -b <database-file> -d  -b <working-dir>')
        sys.exit(2)


if __name__ == '__main__':
    main(sys.argv[1:])
