import csv
from os.path import join

from constant import COURSES, READING_LISTS, CITATIONS
from file.file import File
from utility.leganto_constants import get_csv_course_column_names, get_csv_values_course, \
    get_csv_reading_list_column_names, get_csv_citation_column_names, get_csv_values_reading_list, \
    get_csv_values_citation


class CSVFile(File):

    def __init__(self, organisation, entity, working_dir='./', type=''):
        super().__init__(organisation, working_dir, type)
        csv_path = join(self.working_dir, entity + '_' + self.organisation + '.csv')
        csv_file = open(csv_path, 'w', newline='')
        self.writer = None
        if entity == COURSES:
            self.writer = csv.DictWriter(csv_file, quotechar='"', quoting=csv.QUOTE_NONNUMERIC,
                                         fieldnames=get_csv_course_column_names())
            self.writer.writeheader()
        elif entity == READING_LISTS:
            self.writer = csv.DictWriter(csv_file, quotechar='"', quoting=csv.QUOTE_NONNUMERIC,
                                         fieldnames=get_csv_reading_list_column_names())
            self.writer.writeheader()
        elif entity == CITATIONS:
            self.writer = csv.DictWriter(csv_file, quotechar='"', quoting=csv.QUOTE_NONNUMERIC,
                                         fieldnames=get_csv_citation_column_names())
            self.writer.writeheader()

    def write_course(self, course, counter):
        """Append contents of a course to the courses csv file"""
        self.writer.writerow(get_csv_values_course(self.organisation, course, counter))

    def write_reading_list(self, course_id, reading_list):
        """Appends contents of a reading list to the reading list csv file"""
        self.writer.writerow(get_csv_values_reading_list(self.organisation, course_id,
                                                         reading_list))

    def write_citation(self, course, reading_list_id, citation):
        """write contents of all citations for a reading list to a json file."""
        val = get_csv_values_citation(self.organisation, course, reading_list_id,
                                                     citation)
        self.writer.writerow(val)
