"""Basic file handling support for json/csv"""
import errno
import os
from os.path import join

from constant import COURSES, READING_LISTS, CITATIONS


def create_directory(directory_name):
    try:
        if not os.path.exists(directory_name):
            os.makedirs(directory_name)
    except OSError:
        raise


class File(object):

    def __init__(self, organisation, working_dir='./', type=''):
        self.working_dir = join(working_dir, organisation, type)
        self.organisation = organisation
        self.course_dir = COURSES
        self.reading_list_dir = READING_LISTS
        self.citation_dir = CITATIONS
        self.check_directory_exists_or_make()

    def write_course(self, course, counter):
        """write contents of a course to a json file and appends course to csv file"""
        pass

    def write_reading_list(self, course_id, reading_list):
        """write contents of a reading list to a json file and appends reading list to csv file"""
        pass

    def write_citations(self, course_id, reading_list_id, citations):
        """write contents of all citations for a reading list to a json file."""
        pass

    def write_citation(self, course_as_tuple, reading_list_id, citation):
        """Appends a single citation to citations csv file"""
        pass

    def citations_json_file_exists(self, course_id, reading_list_id):
        return os.path.exists(self.get_json_file_path(course_id, reading_list_id))

    def get_json_file_path(self, course_id, reading_list_id):
        return join(self.working_dir, self.citation_dir, 'course_' + course_id +
                    '_reading_list_' + reading_list_id + "_citations.json")

    def check_directory_exists_or_make(self):
        try:
            create_directory(self.working_dir)
            create_directory(join(self.working_dir, self.course_dir))
            create_directory(join(self.working_dir, self.reading_list_dir))
            create_directory(join(self.working_dir, self.citation_dir))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    def get_course_directory(self):
        return join(self.working_dir, self.course_dir)

    def get_reading_list_directory(self):
        return join(self.working_dir, self.reading_list_dir)

    def get_organisation(self):
        return self.organisation
