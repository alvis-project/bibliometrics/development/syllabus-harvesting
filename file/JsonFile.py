import json
from os.path import join

from file.file import File


class JsonFile(File):

    def __init__(self, organisation, working_dir='./', type=''):
        super().__init__(organisation, working_dir, type)

    def write_course(self, course):
        """write contents of a course to a json file"""
        json_path = join(self.working_dir, self.course_dir, 'course_' + course['id'] +
                         ".json")
        with open(json_path, 'w') as course_file:
            json.dump(course, course_file, indent=4)
            course_file.flush()
            course_file.close()

    def write_reading_list(self, course_id, reading_list):
        """write contents of a reading list to a json file"""
        json_path = join(self.working_dir, self.reading_list_dir, 'course_' + course_id +
                         '_reading_list_' + reading_list['id'] + ".json")
        with open(json_path, 'w') as reading_list_file:
            json.dump(reading_list, reading_list_file, indent=4)
            reading_list_file.flush()
            reading_list_file.close()

    def write_citations(self, course_id, reading_list_id, citations):
        """write contents of a reading list to a json file"""
        json_path = self.get_json_file_path(course_id, reading_list_id)
        with open(json_path, 'w') as citation_file:
            json.dump(citations, citation_file, indent=4)
            citation_file.flush()
            citation_file.close()
