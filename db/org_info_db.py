
class OrganisationDatabase:
    """
       Basic functionality of  SQL for list of third level institutions / organisations and
       institute names.
    """

    def __init__(self, db):
        self.db = db
        self.get_cristin_unit_sql = 'select * from organisations where cristin_unit_id = \'{}\''
        self.get_cristin_unit_by_name_sql = 'select * from organisations where organisation_name_{lang} ' \
                                            '= \'{organisation_name}\''

    def get_cristin_unit(self, cristin_unit_id):
        return self.query_for_one(self.get_cristin_unit_sql.format(cristin_unit_id))

    def get_cristin_unit_by_name(self, organisation_name, lang):
        if lang is 'nb':
            return self.query_for_one(self.get_cristin_unit_by_name_sql.format(
                lang=lang, organisation_name=organisation_name))
        elif lang is 'nn':
            return self.query_for_one(self.get_cristin_unit_by_name_sql.format(
                lang=lang, organisation_name=organisation_name))
        elif lang is 'en':
            return self.query_for_one(self.get_cristin_unit_by_name_sql.format(
                lang=lang, organisation_name=organisation_name))
        else:
            raise "Attempt to retrieve cristin unit with unknown lang {}".format(lang)

    def query_for_one(self, select_statement):
        """Run a query that should only return a single tuple, e.g., primary key select"""
        cursor = self.db.cursor()
        cursor.execute(select_statement)
        result = cursor.fetchone()
        return result

    def query_for_many(self, sql):
        """Run a query that is likely to return multiple tuples."""
        cursor = self.db.cursor()
        cursor.execute(sql)
        return cursor.fetchall()
