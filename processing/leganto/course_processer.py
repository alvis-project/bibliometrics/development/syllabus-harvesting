import re

def process_course_aho(course):
    """aho seems to have two varying processes adding data.
    First example is data like
        '40 140'.	'GK4 Mindre offentlig bygning'
    Second example (as per other organisations)
        UE_189_12 400_1_2020_HØST_1	12 400 - Pre-Diplom (2020-H)
    """
    match = re.search(r"^(\d\d\s\d\d\d\s+)-\s+(.+)$", course['name'])
    if match is not None and len(match.groups()) == 2:
        course['name'] = match.group(2)


def process_course_hio(course):
    process_course_code_title_year(course)


def process_course_him(course):
    # Examples of courses to be processed
    # JUR210 - Innføring i kontraktsrett (2019-V)
    # ADM201 - Innføring i samfunnsvitenskapelig metode (Kristiansund 2019-V)
    # SY16-2 - Sykepleierens faglige og vitenskapelige grunnlag (2019-V)
    # SY16-2 - Sykepleierens faglige og vitenskapelige grunnlag - SpH18(2020-V)
    process_course_code_title_year(course)


def process_course_hvl(course):
    process_course_code_title_year(course)
    match = re.search(r"^(\d\d\d\d-\d\d\s-)(.+)$", course['name'])
    if match is not None and len(match.groups()) >= 2:
        course['name'] = match.group(2)


def process_course_uio(course):
    match = re.search(r"^(\w+\s\w\d\d)(.+)$", course['name'])
    if match is not None and len(match.groups()) >= 2:
        course['name'] = match.group(2)
    else:
        match = re.search(r"^(\w+\s\d\d\w)(.+)$", course['name'])
        if match is not None and len(match.groups()) >= 2:
            course['name'] = match.group(2)


def process_course_usn(course):
    match = re.search(r"^(\d\d\d\d-\d\d\s-\s)(.+)$", course['name'])
    if match is not None and len(match.groups()) >= 2:
        course['name'] = match.group(2)


def process_course_uis(course):
    match = re.search(r"^(\w+\s-\s)(.+)(\s-.+(\s+\(.+\)))$", course['name'])
    if match is not None and len(match.groups()) >= 2:
        course['name'] = match.group(2)
    else:
        match = re.search(r"^(\w+\s-\s)(.+)(\s+\(.+\))$", course['name'])
        if match is not None and len(match.groups()) >= 2:
            course['name'] = match.group(2)


def process_course_nla(course):
    # Try e.g., 3GJ301 - 3GJ301 - Journalism, Media and Globalization (2021-H) (2021-H)
    match = re.search(r"^(.+\s-\s|\w+\s)(.+)((\s+\(.+\))\s(\(.+\)))$", course['name'])
    if match is not None and len(match.groups()) >= 2:
        course['name'] = match.group(2)
    else:
        # Try e.g., 3GJ312 - Global Media Ethics(2022-V)
        match = re.search(r"^(\w+\s)(.+)((\s+\(.+\))|(\s-\s.+\(.+\)))$", course['name'])
        if match is not None and len(match.groups()) >= 3:
            course['name'] = match.group(2)
        else:
            match = re.search(r"^(\w+\s)(.+)$", course['name'])
            if match is not None and len(match.groups()) >= 3:
                course['name'] = match.group(2)


# def process_course_hvo(course):
#     match = re.search(r"^(.+\s-\s|\w+\s)(.+)((\s+\(.+\))|(\s-\s.+\(.+\)))$", course['name'])
#     if match is not None and len(match.groups()) >= 2:
#         course['name'] = match.group(2)
#     else:
#         match = re.search(r"^(\w+\s-\s|\w+\s)(.+)((\s+\(.+\))|(\s-\s.+\(.+\)))$", course['name'])
#         if match is not None and len(match.groups()) >= 2:
#             course['name'] = match.group(2)


def process_course_hvo(course):
    match = re.search(r"^(.+\s-\s|\w+\s)(.+)((\s+\(.+\))|(\s-\s.+\(.+\)))$", course['name'])
    if match is not None and len(match.groups()) >= 2:
        course['name'] = match.group(2)
    else:
        match = re.search(r"^(\w+\s-\s|\w+\s)(.+)((\s+\(.+\))|(\s-\s.+\(.+\)))$", course['name'])
        if match is not None and len(match.groups()) >= 2:
            course['name'] = match.group(2)
        else:
            match = re.search(r"^(\w+\s-\s|\w+\s)(.+)(\(.+\))$", course['name'])
            if match is not None and len(match.groups()) >= 2:
                course['name'] = match.group(2)
            else:
                match = re.search(r"^(\w+\s-\s|\w+\s)(.+)$", course['name'])
                if match is not None and len(match.groups()) >= 2:
                    course['name'] = match.group(2)


def process_course_hk(course):
    process_course_code_title_year(course)
    match = re.search(r"^(\d\d\d\d\s-\s)(.+)$", course['name'])
    if match is not None and len(match.groups()) >= 2:
        course['name'] = match.group(2)
    else:
        match = re.search(r"^(\d\d\d\d\s)(.+)$", course['name'])
        if match is not None and len(match.groups()) >= 2:
            course['name'] = match.group(2)


def process_course_code_title_year(course):
    match = re.search(r"^(.+\s-\s|\w+\s)(.+)((\s+\(.+\))|(\s-\s.+\(.+\)))$", course['name'])
    if match is not None and len(match.groups()) >= 3:
        course['name'] = match.group(2)


def process_course_uib(course):

    # UIB have the following code examples
    # "2022V-JUS264-2-A-0"
    # "2022H-KMD-MUS-217-0"
    # "2022H-MEME306-0"
    # "2022H-UTV ROM 5H-1"
    # "2022H-MAV 1-1"
    # "2022H-EXPHIL-KMSEM-0"
    # "2022H-PHDJUR-V-F-0"
    # "2021H-UTV VISK 5H-1"
    # The year field is often empty so we need to pull it out

    match = re.search(r"^(\d\d\d\d).+$", course['code'])
    if match is not None and len(match.groups()) == 1:
        course['year'] = match.group(1)

    # UIB have a potential issue where the code looks something like 'BAR 2'
    # The actual code should then be 'BAR 2'
    match = re.search(r"^(\d\d\d\d\w)-(.+)-(.+)$", course['code'])
    if match is not None and len(match.groups()) >= 3:
        course['code'] = match.group(2)
