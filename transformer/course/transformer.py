import json
import os
import re

from constant import COURSES, RAW, READING_LISTS, CITATIONS, PROCESS
from downloader import get_course_id_from_link, get_reading_list_id_from_link
from processing.leganto.course_processer import process_course_aho, process_course_uib, \
    process_course_him, process_course_hio, process_course_hk, process_course_hvl, \
    process_course_hvo, process_course_nla, process_course_uio, process_course_uis, \
    process_course_usn, process_course_code_title_year
from utility.leganto_constants import is_digit, semester_to_english
from utility.leganto_utility import find_org_value_information, process_department_values, \
    find_org_desc_information
from utility.string_utility import tidy_string




class Transformer:

    def __init__(self, organisation, org_db):
        self.organisation = organisation
        self.org_db = org_db

    def check_organisation_details(self, citation):
        cristin_unit = self.org_db.get_cristin_unit(citation['institute_code_a'])
        print(cristin_unit)

    def process_courses(self, working_dir):
        print("Processing courses for organisation [{}]".format(self.organisation))
        courses_dir = os.path.join(working_dir, self.organisation, RAW, COURSES)
        if not os.path.isdir(courses_dir):
            print("Nothing to process in {}".format(courses_dir))
            return
        course_files = os.listdir(courses_dir)
        for course_file_name in course_files:
            with open(os.path.join(working_dir, self.organisation, RAW, COURSES, course_file_name),
                      'r') as course_file:
                data = course_file.read()
                course = json.loads(data)
                if "EXLIBRIS_DEFAULT_COURSE" not in course['code']:
                    self.process_course(course)
                print("Processed [{}] course with code [{}], name [{}] "
                      .format(self.organisation, course['code'], course['name']))

    def process_course(self, course):
        self.update_course(course)
        # Make sure the processing hasn't introduced a space at the beginning of the string
        if course['name'] is not None:
            course['name'] = tidy_string(course['name'])
        # Remove links as target json file should not contain any reference to leganto internal
        # links
        if 'link' in course.keys():
            del course['link']

    def process_reading_list(self, course, reading_list):
        # In many cases there is a code attribute in a reading_list that contains a
        # floating point number instead of a course code (Sometimes the actual course code
        # is present)
        if is_digit(reading_list['code']):
            reading_list['code'] = course['code']

        if 'term' in course.keys():
            if len(course['term']) > 3:
                print("Greater than three semesters for a course. Manual inspection required")
                raise "Greater than three semesters for a course. Manual inspection required"
            else:
                for term_count in range(0, len(course['term'])):
                    reading_list['term_name_' + str(term_count + 1)] = course['term'][term_count][
                        'desc']
                    reading_list['term_code_' + str(term_count + 1)] = course['term'][term_count][
                        'value']
        else:
            print("No term")
            reading_list['term_name_1'] = 'unknown'
            reading_list['term_code_1'] = 'unknown'
            reading_list['term_name_2'] = 'unknown'
            reading_list['term_code_2'] = 'unknown'
            reading_list['term_name_3'] = 'unknown'
            reading_list['term_code_3'] = 'unknown'

        reading_list['year'] = course['year']

    def process_reading_lists(self, working_dir):
        print("Processing reading_lists for organisation [{}]".format(self.organisation))

        reading_list_dir = os.path.join(working_dir, self.organisation, RAW, READING_LISTS)
        if not os.path.isdir(reading_list_dir):
            print("Nothing to process in {}".format(reading_list_dir))
            return
        reading_list_files = os.listdir(reading_list_dir)

        for reading_list_file_name in reading_list_files:
            with open(os.path.join(working_dir, self.organisation, RAW, READING_LISTS,
                                   reading_list_file_name),
                      'r') as reading_list_file:
                reading_list = json.loads(reading_list_file.read())
                course_id = get_course_id_from_link(reading_list['link'])
                self.process_reading_list(self.get_course(working_dir, course_id), reading_list)
                print("\t\tProcessed [{}] reading list with id [{}], name [{}] ".format(
                    self.organisation, reading_list['id'], reading_list['name']))

    # Allow for the ability to have common functionality when processing the citations object.
    # Currently, we overwrite the course code from course as it is not always correct
    def process_citations_object(self, course, reading_list_id, citations_object):
        citations_object['code'] = course['code']
        citations_object['course_leganto_code'] = course['course_leganto_code']
        # Set the link value to '' as it is an internal identifier
        if 'link' in citations_object.keys():
            citations_object['link'] = ''

    # Allow for the ability to have common functionality when processing an individual citation.
    # Currently, we delete any URLs that point to internal resources
    def process_single_citation(self, citation):

        # Check that organisation details are correct
        self.check_organisation_details(citation)
        # Set any url value that are internal identifiers to ''
        if 'link' in citation.keys() and citation['link'] is not None:
            citation['link'] = 'removed'
        if 'source' in citation.keys():
            if citation['source'] is not None and 'exlibris' in citation['source']:
                citation['source'] = 'removed'
        if 'source1' in citation.keys():
            if citation['source1'] is not None and 'exlibris' in citation['source1']:
                citation['source1'] = 'removed'
        if 'source2' in citation.keys():
            if citation['source2'] is not None and 'exlibris' in citation['source2']:
                citation['source2'] = 'removed'
        if 'source3' in citation.keys():
            if citation['source3'] is not None and 'exlibris' in citation['source3']:
                citation['source3'] = 'removed'
        if 'source4' in citation.keys():
            if citation['source4'] is not None and 'exlibris' in citation['source4']:
                citation['source4'] = 'removed'
        if 'source5' in citation.keys():
            if citation['source5'] is not None and 'exlibris' in citation['source5']:
                citation['source5'] = 'removed'
        if 'source6' in citation.keys():
            if citation['source6'] is not None and 'exlibris' in citation['source6']:
                citation['source6'] = 'removed'
        if 'source7' in citation.keys():
            if citation['source7'] is not None and 'exlibris' in citation['source7']:
                citation['source7'] = 'removed'
        if 'source8' in citation.keys():
            if citation['source8'] is not None and 'exlibris' in citation['source8']:
                citation['source8'] = 'removed'
        if 'source9' in citation.keys():
            if citation['source9'] is not None and 'exlibris' in citation['source9']:
                citation['source9'] = 'removed'
        if 'source10' in citation.keys():
            if citation['source10'] is not None and 'exlibris' in citation['source10']:
                citation['source10'] = 'removed'
        if 'open_url' in citation.keys():
            if 'exlibris' in citation['open_url']:
                citation['open_url'] = 'removed'
        if 'leganto_permalink' in citation.keys():
            if 'exlibris' in citation['leganto_permalink']:
                citation['leganto_permalink'] = 'removed'
        if 'citation_tags' in citation.keys() and 'link' in citation['citation_tags'].keys():
            citation['citation_tags']['link'] = 'removed'
        if 'metadata' in citation.keys() and 'source' in citation['metadata'].keys():
            if citation['metadata']['source'] is not None and \
                    'exlibris' in citation['metadata']['source']:
                citation['metadata']['source'] = 'removed'

    def process_citations(self, working_dir):
        print("Processing citations for organisation [{}]".format(self.organisation))
        citations_dir = os.path.join(working_dir, self.organisation, RAW, CITATIONS)
        if not os.path.isdir(citations_dir):
            print("Nothing to process in {}".format(citations_dir))
            return
        citation_files = os.listdir(citations_dir)
        for citation_file_name in citation_files:
            with open(os.path.join(working_dir, self.organisation, RAW, CITATIONS,
                                   citation_file_name),
                      'r') as citation_file:
                citations_object = json.loads(citation_file.read())
                course_id = get_course_id_from_link(citations_object['link'])
                reading_list_id = get_reading_list_id_from_link(citations_object['link'])
                course = self.get_course(working_dir, course_id)
                self.update_course(course)
                citations_count = 0
                # Allow for subclass processing of the citations object
                self.process_citations_object(course, reading_list_id, citations_object)
                print("Processed [{}] citations for course code [{}], name [{}] "
                      .format(self.organisation, course['code'], course['name']))

    def update_course(self, course):
        """Undertake pre-processing of course if necessary. It is know that UiT don't
        use underscore for organization identifier 186331400 instead of 186.33.14.00"""

        # Remove any double, single quotes or whitespace at start or end of string
        if course['name'] is not None:
            course['name'] = tidy_string(course['name'])
        # Prefer to have an empty string than None
        else:
            course['name'] = ''

        process_department_values(self.organisation, course)

        org_value = course['academic_department']['value']
        if org_value is not None and org_value.count('.') >= 3:
            org_values = org_value.split('.')
            course['org_code'] = org_values[0]
            course['faculty_code'] = org_values[0] + '.' + org_values[1]
            course['institute_code_a'] = org_values[0] + '.' + org_values[1] + '.' + org_values[2]
            if len(org_values) > 3:
                course['institute_code_b'] = org_values[0] + '.' + org_values[1] + '.' + \
                                             org_values[2] + '.' + org_values[3]
        elif org_value is not None and org_value.count('.') == 1:
            org_values = org_value.split('.')
            course['org_code'] = org_values[0]
            course['faculty_code'] = org_values[0] + '.' + org_values[1]
        elif org_value is not None and org_value.count('.') == 2:
            org_values = org_value.split('.')
            course['org_code'] = org_values[0]
            course['faculty_code'] = org_values[0] + '.' + org_values[1]
            course['institute_code_a'] = org_values[0] + '.' + org_values[1] + '.' + org_values[2]
        elif org_value is not None:
            course['org_code'] = find_org_value_information(self.organisation)
        elif org_value is None:
            if 'academic_department' not in course.keys():
                course['academic_department'] = {}
                course['academic_department'] = {}
            course['academic_department']['value'] = find_org_value_information(self.organisation)
            course['org_code'] = course['academic_department']['value']
            if course['academic_department']['desc'] is None:
                course['academic_department']['desc'] = find_org_desc_information(self.organisation)

        course['course_leganto_code'] = course['code']

        if self.organisation == 'aho':
            process_course_aho(course)
        elif self.organisation == 'uib':
            process_course_uib(course)
        elif self.organisation == 'him':
            process_course_him(course)
        elif self.organisation == 'hio':
            process_course_hio(course)
        elif self.organisation == 'hk':
            process_course_hk(course)
        elif self.organisation == 'hvl':
            process_course_hvl(course)
        elif self.organisation == 'hvo':
            process_course_hvo(course)
        elif self.organisation == 'nla':
            process_course_nla(course)
        elif self.organisation == 'uio':
            process_course_uio(course)
        elif self.organisation == 'uis':
            process_course_uis(course)
        elif self.organisation == 'usn':
            process_course_usn(course)
        else:
            process_course_code_title_year(course)

        self.process_year_and_semester(course)

    def process_year_and_semester(self, course):
        # Sometimes (rarely) searchable_id is not present so try with the code value
        list_searchable_id = [course['course_leganto_code']]

        if course['course_leganto_code'].count("_") < 6 and 'searchable_id' in course.keys():
            list_searchable_id = course['searchable_id']

        for i in range(len(list_searchable_id)):
            searchable_id = list_searchable_id[i]
            # Some cases where additional _1, _2 are present
            if searchable_id.count("_") > 5:
                match = None
                if searchable_id.count("_") == 6:
                    match = re.search(r"(.+)_(.+)_(.+)_(.+)_(.+)_(.+)_(.+)", searchable_id)
                elif searchable_id.count("_") == 7:
                    match = re.search(r"(.+)_(.+)_(.+)_(.+)_(.+)_(.+)_(.+)_(.+)", searchable_id)

                if match is not None:
                    self.process_year(course, match)
                    self.process_semester(course, match)
                else:
                    print("**** Problem processing searchable_id " + searchable_id)

    def process_year(self, course, match):
        if match is not None and len(match.groups()) >= 6:
            course_code = match.group(3)
            course['code'] = course_code

            if course['year'] is None:
                course['year'] = match.group(5)
            elif len(course['year']) < 1:
                course['year'] = match.group(5)

    def process_semester(self, course, match):
        # Term is missing so pull it from the regex from leganto code
        if 'term' not in course.keys():
            course_semester = match.group(6).lower()
            course['term'] = []
            if course_semester == 'vår':
                course['term'].append({'desc': course_semester, 'value': 'spring'})
            elif course_semester == 'høst':
                course['term'].append({'desc': course_semester, 'value': 'autumn'})
            elif course_semester == 'som':
                course['term'].append({'desc': course_semester, 'value': 'summer'})
        else:
            # Make sure only english words are used for semester values
            for term_count in range(0, len(course['term'])):
                course['term'][term_count]['desc'] = semester_to_english(course['term'][
                                                                             term_count]['desc'])
                course['term'][term_count]['value'] = semester_to_english(course['term'][
                                                                              term_count]['value'])

    def get_course(self, working_dir, course_id):
        """Note this gets course from the processed directory, not the raw directory!"""
        with open(os.path.join(working_dir, self.organisation, PROCESS, COURSES,
                               'course_' + course_id + ".json"), 'r') as course_file:
            return json.loads(course_file.read())
