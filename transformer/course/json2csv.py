from constant import PROCESS
from file.CSVFile import CSVFile
from file.file import File
from transformer.course.transformer import Transformer
from utility.leganto_constants import check_for_unknown_reading_list_keys, \
    check_for_unknown_citation_keys


class Json2CSV(Transformer):

    def __init__(self, organisation, entity, working_dir, output_dir):
        super().__init__(organisation)
        self.working_dir = working_dir
        self.output_dir = output_dir
        self.file = CSVFile(organisation, entity, working_dir, PROCESS)
        self.counter = 0
        self.entity = entity

    def process_course(self, course):
        super().process_course(course)
        self.file.write_course(course, self.counter)
        self.counter = self.counter + 1

    def process_reading_list(self, course, reading_list):
        super().process_reading_list(course, reading_list)
        self.file.write_reading_list(course['id'], reading_list)
        check_for_unknown_reading_list_keys(reading_list)

    def process_single_citation(self, citation):
        super().process_single_citation(citation)

    def process_citations_object(self, course, reading_list_id, citations_object):
        super().process_citations_object(course, reading_list_id, citations_object)
        if 'citations' in citations_object:
            citations = citations_object['citations']
            citations_count = 0
            if 'citation' in citations:
                for citation in citations['citation']:
                    citations_count = citations_count + 1
                    self.process_single_citation(citation)
                    # if get_string_value_sub_dict(citation, 'metadata', 'title') is not None:
                    #     print(
                    #         "Processing [{}] course [{}], reading list[{}] citation[{}], "
                    #         "name [{}] ".format(self.organisation, course_id,
                    #                             reading_list_id, citation['id'],
                    #                             get_string_value_sub_dict(citation,
                    #                                                       'metadata',
                    #                                                       'title')))
                    # else:
                    #     print(
                    #         "Processing [{}] course [{}], reading list[{}] citation[{}], "
                    #         "name [{}] "
                    #             .format(self.organisation, course_id, reading_list_id,
                    #                     citation['id'],
                    #                     get_string_value_sub_dict(citation, 'metadata',
                    #                                               'article_title')))
                    self.file.write_citation(course, reading_list_id, citation)
                    check_for_unknown_citation_keys(citation)
            print("Processed [{}] citations for [{}], course [{}], reading list[{}]".format(
                str(citations_count), self.organisation, course['id'], reading_list_id))
