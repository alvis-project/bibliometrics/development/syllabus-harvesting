from constant import PROCESS
from file.JsonFile import JsonFile
from file.file import File
from transformer.course.transformer import Transformer


class Json2Json(Transformer):

    def __init__(self, organisation, working_dir, output_dir):
        super().__init__(organisation)
        self.working_dir = working_dir
        self.output_dir = output_dir
        self.file = JsonFile(organisation, working_dir, PROCESS)

    def process_course(self, course):
        super().process_course(course)
        self.file.write_course(course)

    def process_reading_list(self, course, reading_list):
        super().process_reading_list(course, reading_list)
        reading_list['blocked'] = False
        if 'id' not in reading_list.keys():
            print("Course with id {} has reading list with name {} that is unavailable".format(
                course['id'], reading_list['name']))
            reading_list['blocked'] = True
        self.file.write_reading_list(course['id'], reading_list)

    def process_single_citation(self, citation):
        super().process_single_citation(citation)

    def process_citations_object(self, course, reading_list_id, citations_object):
        super().process_citations_object(course, reading_list_id, citations_object)
        self.file.write_citations(course['id'], reading_list_id, citations_object)