import getopt
import os
import sys
from datetime import datetime

from db.org_info_db import OrganisationDatabase

sys.path.insert(0, './transformer/course/*')
from constant import CITATIONS, READING_LISTS, COURSES, ALL, CSV, JSON, DB  # noqa: E402
from transformer.course.json2csv import Json2CSV  # noqa: E402
from transformer.course.json2db import Json2Db   # noqa: E402
from transformer.course.json2json import Json2Json   # noqa: E402


def get_options_retrieve(argv):
    try:
        # Set some default values that will be used unless they are overridden by command line
        entity = COURSES
        # Assume all organisations to be processed
        organisation = ALL
        # Using year/month as directory as download processing of everything can go over a number
        # of days. Expected use of the script i bi-yearly.
        working_dir = str(datetime.today().year) + "-" + str(datetime.today().month)
        # Assume syllabus.sqlite as default filename
        db_file = 'syllabus.sqlite'
        # Assume we will want to transform to cleaned JSON
        file_format = JSON
        opts, args = getopt.getopt(argv, 'he:o:f:d:w:')
        for opt, arg in opts:
            if opt == '-h':
                print(
                    'transform.py -e <entity> -o <organisation> -f <format> -d <database-file> -w '
                    '<working-dir>')
                sys.exit()
            if opt in '-e':
                entity = arg
            if opt in '-o':
                organisation = arg
            if opt in '-f':
                file_format = arg
            if opt in '-d':
                db_file = arg
            if opt in '-w':
                working_dir = arg

        return entity, organisation, file_format, db_file, working_dir

    except getopt.GetoptError:
        print('transform.py -e <entity> -o <organisation> -f <format> -d <database-file> -w '
              '<working-dir>')
        sys.exit(2)


def get_transformer(organisation, file_format, entity, db_file, working_dir, org_db):
    # Note currently the input and output directories are the same. This is to keep things simple
    # It might be worthwhile later allowing users to separate them, but not for now
    if file_format == DB:
        return Json2Db(db_file, organisation, org_db)
    elif file_format == CSV:
        return Json2CSV(organisation, entity, working_dir, working_dir, org_db)
    elif file_format == JSON:
        return Json2Json(organisation, working_dir, working_dir, org_db)
    else:
        print("Cannot process {} for organisation [{}] to format [{}]".format(entity,
                                                                              organisation,
                                                                              file_format))
        raise


def main(argv):
    entity_to_process, organisation_to_process, file_format, db_file, working_dir = \
        get_options_retrieve(argv)

    org_db = OrganisationDatabase(os.path.join(working_dir, 'organisation.sqlite'))

    # We assume any directory in the working directory corresponds to a third level organisation
    if organisation_to_process == ALL:
        organisations = os.listdir(working_dir)
    else:
        organisations = [organisation_to_process]



    for organisation in organisations:
        transformer = get_transformer(organisation, file_format, entity_to_process, db_file,
                                      working_dir, org_db)
        if entity_to_process == COURSES or entity_to_process is None:
            print("Processing courses for organisation [{}]".format(organisation))
            transformer.process_courses(working_dir)
        elif entity_to_process == READING_LISTS:
            print("Processing reading lists for organisation [{}]".format(organisation))
            transformer.process_reading_lists(working_dir)
        elif entity_to_process == CITATIONS:
            print("Processing citations for organisation [{}]".format(organisation))
            transformer.process_citations(working_dir)
        else:
            print("Unknown option [{}]. Unable to continue.".format(entity_to_process))

    sys.exit()


if __name__ == '__main__':
    main(sys.argv[1:])
